<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Auth::routes(['verify' => true]);

Route::get('/', function (){
    //Chequeo si esta logeado, si lo esta lo envio al home, sino al login
    if(\Auth::check()){
        return redirect(route('home'));
    }
    else{
        return view('auth.login');
    }
});

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');


//Rutas para el usuario
Route::middleware(['auth','cliente'])->group(function () { 
    Route::get('/misdatos', function () {
        return view('usuario.datos'); 
    })->name('misdatos');
    Route::get('/veterinaria', function () { 
        return view('usuario.veterinaria'); 
    })->name('veterinaria');
    Route::get('/getUsuario/{idUsuario}', 'UsuarioController@getUsuario')->name('getUsuario');
    Route::get('/getMascota/{idMascota}', 'MascotasController@getMascota')->name('getMascota');
    Route::get('/listaMascotas', "MascotasController@listaMascotas")->name("listaMascotas");
});


Route::get('/listaMascotas', "MascotasController@listaMascotas")->name("listaMascotas")->middleware('verified','auth');

//Rutas para el administrador
Route::middleware(['auth','administrador'])->group(function (){
    Route::get('/nuevaMascota','MascotasController@cargarMascota')->name('cargarMascota');
    Route::post('/nuevaMascota','MascotasController@nuevaMascota')->name('nuevaMascota');
    // Route::get('/getMascota','MascotasController@getMascota')->name('getMascota');
    Route::get('/eliminarMascota','MascotasController@eliminarMascota')->name('eliminarMascota');
    Route::post('/modificarMascota','MascotasController@modificarMascota')->name('modificarMascota');

    Route::get('/editarObservaciones/{id_mascota}','MascotasController@editarObservaciones')->name('editarObservaciones');
    Route::post('/guardarObservaciones','MascotasController@guardarObservaciones')->name('guardarObservaciones');



    //Rutas relacionadas a los usuarios
    Route::get('/crearUsuario', 'UsuarioController@crearUsuario_view')->name('crearUsuario_view');
    Route::post('/crearUsuario', 'UsuarioController@crearUsuario')->name('crearUsuario');
    Route::get('/editarUsuario/{id_usuario}', 'UsuarioController@editarUsuarioView')->name('editarUsuarioView');
    Route::post('/editarUsuario', 'UsuarioController@editarUsuario')->name('editarUsuario');
    Route::post('/eliminarUsuario', 'UsuarioController@eliminarUsuario')->name('eliminarUsuario');
    // Route::post('/getUsuario', 'UsuarioController@getUsuario')->name('getUsuario');
    Route::get('/listaDeClientes', 'UsuarioController@listaDeClientes')->name('listaDeClientes');
    Route::get('/getMascotasUsuario/{id_usuario}','UsuarioController@getMascotasUsuario')->name('getMascotasUsuario');

    /*
        Estas rutas son las que se usan con AJAX para buscar los usuarios. 
        {dni?} significa que recibe un parametro llamado dni 
        y el ? indica que puede ser opcional el parametro.
    */
    Route::get('/buscarUsuarioAJAX/{apellido?}', "UsuarioController@buscarUsuarioAJAX")->name("buscarUsuarioAJAX");
    Route::get('/listarUsuariosAJAX/{apellido?}', 'UsuarioController@listarUsuariosAJAX')->name('listarUsuariosAJAX');

    Route::get('/listaMascotasAJAX/{id_usuario?}', "MascotasController@listaMascotasAJAX")->name("listaMascotasAJAX");

    //Rutas relacionadas a las vacunas
    Route::get('/getVacunas/{id_mascota}', 'VacunasController@getVacunas')->name('getVacunas');
    Route::get('/cargarVacuna/{id_vacuna?}/{id_mascota}',"VacunasController@cargarVacuna")->name("cargarVacuna");
    Route::post('/registrarVacuna',"VacunasController@registrarVacuna")->name("registrarVacuna");
    Route::post('/eliminarVacuna','VacunasController@eliminarVacuna')->name('eliminarVacuna');
    Route::get('/index', 'VacunasController@getVacunasProgramadas')->name('getVacunasProgramadas');
    Route::get('/vacunasProgramadas', 'VacunasController@getVacunasProgramadas')->name('getVacunasProgramadas');
    

    //Rutas relacionadas a las desparasitaciones 
    Route::get('/getDesparasitaciones/{id_mascota}',"DesparasitacionesController@getDesparasitaciones")->name("getDesparasitaciones");
    Route::post('/nuevaDesparasitacion',"DesparasitacionesController@nuevaDesparasitacion")->name("nuevaDesparasitacion");    
    Route::get('/cargarDesparasitacion/{id_desparasitacion?}/{id_mascota}',"DesparasitacionesController@cargarDesparasitacion")->name("cargarDesparasitacion");
    Route::get('/eliminarDesparasitacion/{id_desparasitacion}',"DesparasitacionesController@eliminarDesparasitacion")->name("eliminarDesparasitacion");
    
    //Rutas relacionadas a los recordatorios                
    Route::post('/cargarRecordatorio2',"RecordatoriosController@cargarRecordatorio2")->name("cargarRecordatorio2");    
    Route::get('/eliminarRecordatorio/{id_usuario}/{tipo}',"RecordatoriosController@eliminarRecordatorio")->name("eliminarRecordatorio");
    
    // Rutas para gestion de turnos
    Route::get('/agendarTurno', 'TurnosController@AgendarTurnoView')->name("agendarTurnoView");
    Route::post('/agendarTurno', 'TurnosController@AgendarTurno')->name("agendarTurno");
    Route::get('/getTurnos', 'TurnosController@getTurnosPorDia')->name("getTurnosPorDia");
    Route::get('/getTurnosAJAX/{fecha?}', 'TurnosController@getTurnosPorDiaAJAX')->name("getTurnosPorDiaAJAX");
    Route::post('/eliminarTurno', 'TurnosController@eliminarTurno')->name("eliminarTurno");
});

Route::get('/tallas','MascotasController@tallas');