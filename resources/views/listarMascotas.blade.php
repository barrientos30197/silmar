@extends('layouts.app')

@section('content')

    <form method="GET" id="form">
        <select id="selectUsuario">
        <option disabled selected> Seleccione un usuario </option>
             @foreach ($usuarios as $usuario)
             <option value={{$usuario->id}}> {{$usuario->nombre}} {{$usuario->apellido}} </option>
             @endforeach
        </select>
    </form>


    <div id="listaMascotas">
        @foreach ($mascotas as $mascota)
            {{$mascota->nombre}} 
            {{$mascota->especie}}
            {{$mascota->peso}} 
        @endforeach
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
        /*
            Esta funcion detecta cambios en el select de Usuarios y llama a la ruta de 'listarMascotas' para tal usuario
        */
        $('#selectUsuario').on('change', function(){
            var seleccionado=$('#selectUsuario').val();
            var url='{{route('listaMascotasAJAX')}}';
           
            $.ajax({
                type: 'GET',
                url: url + '?id_usuario=' + seleccionado, // Agrego el parametro id_usuario a la ruta para poder buscar las mascotas del mismo.
            }).done(function(data) {
                $( "#listaMascotas" ).html( data ); //El resultado debe ir al div 'listaMascotas'
            });
        });
    </script>
@endsection
