<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="{{ url('/nuevaMascota')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field()}}
        <input type="text" name="dniCliente" placeholder="Dni cliente: " required>
        <input type="text" name="nombre" placeholder="Nombre: " required>
        <input type="text" name="especie" placeholder="Especie: "required>
        <input type="text" name="raza" placeholder="Raza: "required>
        <input type="number" name="sexo" placeholder="Id Sexo: " min="0" max="1" step="1" required>
        <input type="text"  name="color" placeholder="Color: " required>
        <input type="file"  name="nombre_imagen" placeholder="Imagen">
        <input type="number" step="0.01"name="peso" placeholder="Peso" required>
        <!--<input type="number" name="id_usuario" placeholder="Id Usuario: " required>-->
        <input type="number" name="id_pelaje" placeholder="Id Pelaje: " required>
        <input type="number" name="id_talla" placeholder="Id Talla: " required>
        <input type="submit" value="Enviar">
    
</form>
    
</body>
</html>