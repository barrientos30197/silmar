<!--
    Esta vista muestra los antiparasitarios de una mascota. 
-->

@extends('admin.layout')

@section('content')

<section id="main-content" >
    <section class="wrapper">

        <div class="row">            
            <div class="col-lg-12">
                @if(session('desparasitacionEliminada'))
                    <span></span>
                    <div class="alert-success form-control" style="text-align:center; font-size:120%"> 
                        {{ session('desparasitacionEliminada') }}
                    </div>                  
                    <br>
                @elseif(session('desparasitacionNoEliminada'))
                    <span></span>
                    <div class="alert-danger form-control"> {{ session('desparasitacionNoEliminada') }}</div>
                    <br>
                @endif

                <section class="panel">                                            
                    <header class="panel-heading"  style="font-size:120%"><i class="fas fa-capsules"></i>
                        Antiparasitarios de 
                        @foreach ($mascota as $m)
                            {{$m->nombre}} 
                            <?php $idMascota = $m->id_mascota ?>                           
                        @endforeach
                    </header>              
                    
                    <table class="table">
                        <thead>
                            <tr>    
                                <th></th>
                                <th>Marca</th>
                                <th>Dosis</th>
                                <th>Fecha</th>
                                <th>Renovación</th> 
                                <th>Editar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>                            
                            @foreach ($desparasitaciones as $desparasitacion)                                
                                <tr>  
                                    <td></td>
                                    <td>{{$desparasitacion->marca}} </td>
                                    <td>{{$desparasitacion->dosis}} </td>
                                    <td>{{$desparasitacion->fecha_desparasitacion}} </td>
                                    <td>{{$desparasitacion->fecha_renovacion}} </td>            
                                    <td>                                                                                        
                                        <a href="{{route('cargarDesparasitacion',  [$desparasitacion->id_desparasitacion, $idMascota])}}" class="btn btn-primary btn-sm">
                                            <i class="fas fa-pencil-alt" style="font-size:18px"></i>
                                        </a>                                            
                                    </td>
                                    <td>
                                        <a onclick="eliminar({{$desparasitacion->id_desparasitacion}})" href="#" class="btn btn-danger">
                                            <i class="fas fa-trash-alt" style="font-size:18px"></i>
                                        </a>                                            
                                    </td> 
                                </tr>                                                                
                            @endforeach
                        </tbody>                        
                    </table>                    
                </section>
            </div>
        </div>                        
        
        <div style="text-align:center">
            <a href="{{route('cargarDesparasitacion', [0, $idMascota])}}" class="btn btn-primary btn-lg">
                Agregar Nuevo Antiparasitario                
            </a>
        </div>
        <div class="col-md-12" style="text-align:center">
          {{$desparasitaciones->render()}}
        </div>
    </section>
</section>
@endsection

@section('scripts')
    <script>
        function eliminar($id){
            if(confirm("¿Está seguro que desea eliminar este antiparasitario?")){
                window.location.pathname = "eliminarDesparasitacion/"+$id;      
            }
        }
    </script>
@endsection
