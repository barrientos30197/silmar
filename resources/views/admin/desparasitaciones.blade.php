<!--
  Esta vista muestra un formulario para cargar o editar antiparasitarios. 
-->

@extends('admin.layout')

@section('content')
 
<section id="main-content" >
    <section class="wrapper">

      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <header class="panel-heading" style="font-size:120%"><i class="fas fa-capsules"></i>
              Añadir antiparasitario
            </header>            
            <div class="panel-body">
              <form action="{{ route('nuevaDesparasitacion')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field()}}
                  
                @if ($desparasitacion != NULL)
                  <input type="hidden" id="id_desparasitacion" name="id_desparasitacion" value={{$desparasitacion->id_desparasitacion}}>
                @endif
                <div class="form-group">
                  <label class="col-lg-2 control-label">Mascota</label>
                  <div class="col-sm-8">
                    @foreach ($mascota as $m)
                      {{$m->nombre}}                        
                      <input type="hidden" id="id_mascota" name="id_mascota" value={{$m->id_mascota}} required>
                    @endforeach
                  </div>
                </div>                
                
                <div class="form-group">
                  <label class="col-lg-2 control-label"> Marca </label>
                  <div class="col-sm-8">
                    @if ($desparasitacion != NULL) 
                      <input type="text" class="form-control" name="marca" id="marca" value={{$desparasitacion->marca}}>
                    @else
                      <input type="text" class="form-control" name="marca" id="marca">
                    @endif
                  </div>
                </div>
                                  
                <div class="form-group">
                  <label class="col-lg-2 control-label">Dosis </label>
                  <div class="col-sm-8">
                    @if ($desparasitacion != NULL)
                      <input type="text" class="form-control" name="dosis" id="dosis" value={{$desparasitacion->dosis}} >
                    @else
                      <input type="text" class="form-control" name="dosis" id="dosis">
                    @endif                    
                  </div>                   
                </div>                                 
                
                <div class="form-group">
                  <div class="row">
                    <label class="col-lg-2 control-label">Fecha: <font color= red><strong>*</strong></font></label>
                    <div class="col-sm-3">
                      @if ($desparasitacion != NULL)
                        <input type="date" class="form-control" value={{$desparasitacion->fecha_desparasitacion}} name="fecha_desparasitacion"  required>
                      @else
                        <input type="date" class="form-control" name="fecha_desparasitacion"  required>
                      @endif
                    </div>                                       
                  
                    <label class="col-lg-2 control-label">Renovación: <font color= red><strong>*</strong></font></label>
                    <div class="col-sm-3">
                      @if ($desparasitacion != NULL)
                        <input type="date" class="form-control" value={{$desparasitacion->fecha_renovacion}} name="fecha_renovacion"  required>
                      @else
                        <input type="date" class="form-control" name="fecha_renovacion"  required>
                      @endif
                    </div>                   
                  </div> 
                </div>
                
                <div class="form-group">
                  <label class="col-sm-10 control-label"></label>
                  <div class="col-sm-2">                                        
                    <input type="submit" class="btn btn-primary  btn-lg" value="Guardar">                                        
                  </div>                  
                </div>

                @if(session('desparasitacionCreada'))
                  <span></span>
                  <div class="alert-success form-control"> {{ session('desparasitacionCreada') }}</div>                  
                @endif
                @if(session('desparasitacionNoCreada'))
                  <span></span>
                  <div class="alert-danger form-control"> {{ session('desparasitacionNoCreada') }}</div>
                @endif

              </form> 
            </div>
          </section>
        </div>
      </div>
      
      <div style="text-align:center">
        @foreach ($mascota as $m)                                          
          <a href="{{route('getDesparasitaciones', $m->id_mascota)}}" class="btn btn-primary btn-lg">
            Ver Antiparasitarios de {{$m->nombre}}  
          </a>
        @endforeach
      </div>
      

    </section>
</section>
@endsection
