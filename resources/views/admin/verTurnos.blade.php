@extends('admin.layout')

@section('content')
<section id="main-content" >
    <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <section class="panel">
                        <header class="panel-heading">
                            <h3>Turnos</h3>
                        </header>

                        <div class="panel body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th style="text-align: end"><h4><strong>Ingrese el dia</strong></h4></th>

                                    <th><input type="date" name="fecha" id="fecha" class="form-control" placeholder="Ingrese la fecha" > </th>

                                    {{-- El icono de buscar lo sacaria porque al hacerlo con ajax no va a ser necesario apretarlo --}}
                                    <th>
                                    <a onclick="buscarUsuarioAJAX();"  class="btn btn-primary btn-sm"><i class="fa fa-search fa-2x"></i></a>
                                    </th>
                                </tr>
                                        
                            
                                </thead>
                            </table>  
                        </div> 
                    
                        <div class="panel-body">
                            <table class="table  table-striped">
                                <thead>
                                
                                <tr>
                                    <th>Hora</th>
                                    <th>Cliente</th>
                                    <th>Descripcion</th>
                                    <th>Acciónes</th>
                                </tr>
                                </thead>
                                <tbody id="tbody">
                                    <tr id="fila-inicio"> <td colspan="3">Primero seleccione una fecha para ver los turnos</td></tr>
                                </tbody>
                            </table>

                        </div>
                        <div id="alert" class="form-control d-none" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            <div id="mensaje">
                            </div>
                            <a class="btn btn-primary btn-lg" href="{{route('agendarTurnoView')}}" > Agendar nuevo turno </a>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
</section>

@endsection

@section('scripts')

    <script>
        $("#fecha").on("change",function(){
            var fecha = $("#fecha").val();

            $("#fila-inicio").remove();
            
            $.ajax({
                url: '{{route("getTurnosPorDiaAJAX")}}/'+fecha,
                type: 'get',
                dataType: 'json',

                success: function(response){
                    if(response['success']=="true"){
                        $("#tbody").empty();

                        if(Object.keys(response['turnos']).length == 0){
                            $('#tbody').append('<tr> <td>No hay turnos para la fecha seleccionada.</td></tr>');
                        }
                        else{
                            $.each( response['turnos'], function( key, value ) {
                                $('#tbody').append('<tr id="turno'+value['id_turno']+'"> <td>'+value["hora_turno"]+'</td><td>'+value["cliente"]+'</td><td>'+value["descripcion"]+'</td><td><button class="btn btn-primary btn-eliminar" data-id="'+value['id_turno']+'">Eliminar</button></td></tr>');
                            });
                        }
                    }
                    else{
                        $('#tbody').append('<tr> <td colspan="3">'+value["mensaje"]+'</tr>');
                    }
                },
            });
        });

        $(document).on("click", ".btn-eliminar", function() {
            if(confirm("¿Está seguro que desea eliminar este turno?")){
                var token = '{{csrf_token()}}';
                var id_turno = $(this).attr("data-id");

                $.ajax({
                    url: '{{route("eliminarTurno")}}',
                    type: 'post',
                    data: {
                        id_turno: id_turno,
                        _token: token
                    },
                    dataType: 'json',
                    
                    success: function(response){
                        $("#mensaje").empty();
                        $("#alert").removeClass("alert-success");
                        $("#alert").removeClass("alert-danger");

                        if(response['success']=="true"){
                            $("#alert").addClass("alert-success");
                        }
                        else{
                            $("#alert").addClass("alert-danger");
                        }
                        $("#alert").removeClass("d-none");
                        $("#mensaje").text(response['mensaje']);
                    },
                    complete:function(data){
                        $("#turno"+id_turno).remove();
                    }
                });
            }
        });

        $(document).on("click", "agendarTurno", function() {
            var fecha = $("#fecha").val();
            if(confirm("¿Está seguro que desea agendar un turno para el "+fecha +"?")){
                var token = '{{csrf_token()}}';
                $.ajax({
                    url: '{{route("agendarTurno")}}',
                    type: 'post',
                    data: {
                        fecha: fecha,
                    },
                    dataType: 'json',
                    
                    success: function(response){
                        $("#mensaje").empty();
                        $("#alert").removeClass("alert-success");
                        $("#alert").removeClass("alert-danger");

                        if(response['success']=="true"){
                            $("#alert").addClass("alert-success");
                        }
                        else{
                            $("#alert").addClass("alert-danger");
                        }
                        $("#alert").removeClass("d-none");
                        $("#mensaje").text(response['mensaje']);
                    },
                    complete:function(data){
                        $("#turno"+id_turno).remove();
                    }
                });
            }
        });
    </script>

@endsection