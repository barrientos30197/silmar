@extends('admin.layout')

@section('content')
 
<section id="main-content" >
    <section class="wrapper">

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                      Datos de la mascota: 
                    </header>
                    <div class="panel-body">
                     <form action="{{ url('/nuevaMascota')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                       {{ csrf_field()}}
                       <div class="form-group">
                         <label class="col-lg-2 control-label">Cliente <font color= red><strong>*</strong></font></label>
                         <div class="col-sm-8">                           
                            <select class="selectpicker show-menu-arrow" 
                                    data-style="form-control" 
                                    data-live-search="true" 
                                    title="-- Seleccionar al cliente --"
                                    multiple="multiple"
                                    name="dniCliente">
                              @foreach ($usuarios as $usuario) 
                                <option value={{$usuario->dni}}>{{$usuario->apellido}} {{$usuario->nombre}}   ({{$usuario->dni}})</option>      
                              @endforeach
                            </select>
                         </div>
                       </div>
   
                       <div class="form-group">
                         <label class="col-lg-2 control-label">Nombre de la mascota <font color= red><strong>*</strong></font></label>
                         <div class="col-sm-8">
                            <input type="text" class="form-control" name="nombre"  required>
                         </div>
                       </div>
   
                                     
                       <div class="form-group">
                        <label class="col-lg-2 control-label">Sexo  <font color= red><strong>*</strong></font></label>
                        <div class="col-sm-8">
                          <select name="sexo" class="form-control">
                            <option selected disabled>Sexo</option>
                            <option value=1>Macho</option>
                            <option value=2>Hembra</option>
                          </select>
                        </div> 
                      
                      </div> 
                      
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Especie <font color= red><strong>*</strong></font></label>
                        <div class="col-sm-8">
                          <select name="especie" class="form-control">
                            <option selected disabled>Seleccione una opcion</option>
                            <option value="felino">Felino</option>
                            <option value="canino">Canino</option>
                          </select>
                        </div> 
                    </div>

                          
                    <div class="form-group">
                      <label class="col-lg-2 control-label">Raza <font color= red><strong>*</strong></font></label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="raza" required>
                      </div>
                    </div>
                       
                       <div class="form-group">
                         <label class="col-lg-2 control-label">Color <font color= red><strong>*</strong></font></label>
                         <div class="col-sm-8">
                           <input type="text" class="form-control" name="color" required>
                         </div>
                       </div>
                                        
   
                       <div class="form-group">
                         <label class="col-lg-2 control-label">Pelaje <font color= red><strong>*</strong></font></label>
                         <div class="col-sm-8">
                           <select name="id_pelaje" class="form-control">
                             <option selected disabled>Pelaje</option>
                                @foreach($pelajes as $pelaje)
                                     <option value={{$pelaje->id_pelaje}}>{{$pelaje->medida}}</option>
                                 @endforeach
                           </select>
                           @if ($errors->has('id_pelaje'))
                               <p>{{ $errors->first('id_pelaje') }}</p>
                           @endif
   
                         </div> 
                       </div>
   
   
                       <div class="form-group">
                         <label class="col-lg-2 control-label">Talla <font color= red><strong>*</strong></font></label>
                         <div class="col-sm-8">
                           <select name="id_talla" class="form-control">
                             <option selected disabled>Talla</option>
                                @foreach($tallas as $talla)
                                     <option value={{$talla->id_talla}}>{{$talla->medida}}</option>
                                 @endforeach
                           </select>
                           @if ($errors->has('id_talla'))
                               <p>{{ $errors->first('id_talla') }}</p>
                           @endif
   
                         </div> 
                       
                       </div>

                      <div class="form-group">
                        <label class="col-lg-2 control-label">Peso <font color= red><strong>*</strong></font></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" step="0.01"name="peso"  required>
                        </div>
                      </div>

                       <div class="form-group">
                         <label class="col-sm-10 control-label"></label>
                             <div class="col-sm-2">
                            <input type="submit" class="btn btn-primary  btn-lg" value="Guardar">
                         </div>
                      </div>
                      
                  @if(session('mensaje_ok'))
                    <span></span>
                    <div class="alert-success form-control"> {{ session('mensaje_ok') }}</div>
                  @endif
                  @if(session('mensaje_error'))                                      
                    <span></span>
                    <div class=" alert-danger">{{ session('mensaje_error')}}</div>
                  @endif                      
               </form>                
            </div>
          </section>
        </div>
      </div>            
    </section>
</section>
@endsection

@section('scripts')
  <script>
  /* Multiple Item Picker */
    $('.selectpicker').selectpicker({
      style: 'btn-default'
    });
  </script>
@endsection