
@extends('admin.layout')

@section('content')

<section id="main-content" >
  <section class="wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fas fa-id-card"></i>Datos del Cliente</h3>
      </div>
    </div> 

    <div class="row">            
      <div class="col-lg-12">
        <section class="panel">                                            
          <header class="panel-heading"  style="font-size:120%"><i class="fas fa-user"></i>
            {{$usuario}}
          </header>     
      
          <table class="table">
            <thead>        
              <tr>
                <th>Nombre</th>
                <th>Especie</th>
                <th>Raza</th>
                <th>Sexo</th>
                <th>Color</th>
                <th>Peso</th>
                <th>Vacunas</th>
                <th>Antiparasitarios</th>     
                <th>Historia Clinica</th>   
              </tr>
            </thead>

            <tbody id="tbody">
              @foreach ($mascotas as $mascota)
                <tr>  
                  <th>{{$mascota->nombre}} </th>
                  <th>{{$mascota->especie}} </th>
                  <th>{{$mascota->raza}} </th>
                  <th>@if ($mascota->sexo == 1) Macho @else Hembra @endif </th>
                  <th>{{$mascota->color}} </th>
                  <th>{{$mascota->peso}} </th>                  
                  <th> <a class="btn btn-primary" href="{{route('getVacunas',$mascota->id_mascota)}}">Ver</a> </th>          
                  <th><a class="btn btn-primary" href="{{route('getDesparasitaciones',$mascota->id_mascota)}}">Ver</a></th>
                  <th>  
                  <a class="btn btn-primary" href="{{ route('editarObservaciones',$mascota->id_mascota)}}">
                      @if ($mascota->observaciones == null) +  @else Ver @endif </a>
                       
        
                  <th>                    
                </tr>
              
                                              

              @endforeach
            </tbody>
          </table>
        </section>

      </div>
    </div>
    <div class="col-md-12" style="text-align:center">
      {{$mascotas->render()}}
    </div>
  </section>
</section>

@endsection


