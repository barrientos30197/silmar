@extends('admin.layout')

@section('content')
 
<section id="main-content" >
    <section class="wrapper">

        <div class="row">
            <div class="col-lg-12">
              <h3 class="page-header"><i class="fa fa-file-text-o"></i> Editar datos del cliente: </h3>
            </div>
         </div>
        
      <div class="row">
           <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                  Datos del cliente: 
                </header>
                <div class="panel-body">
                  <form method="POST" enctype="multipart/form-data" action="{{route('editarUsuario')}}" class="form-horizontal">
                    @csrf
                    
                    <input type="hidden" value="{{$usuario->id}}" name="id_usuario">
                    <input type="hidden" value="{{$usuario->password}}" name="password">
                    <div class="form-group">
                      <label for="dni" class="col-sm-2 control-label">Dni</label>
                      <div class="col-sm-8">
                        <input id="dni" type="text" class="form-control @error('dni') is-invalid @enderror" name="dni" value="{{$usuario->dni}}" required readonly>

                        @error('dni')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>

                    <div class="form-group">                    
                      <label for="nombre" class="col-sm-2 control-label">Nombre <font color= red><strong>*</strong></font></label>
                      <div class="col-sm-8">
                        <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{$usuario->nombre}}" required> 

                        @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                      </div>
                    </div>

                    <div class="form-group">
                      <label for="apellido" class="col-sm-2 control-label">Apellido <font color= red><strong>*</strong></font></label>
                      <div class="col-sm-8">
                        <input id="apellido" type="text" class="form-control @error('apellido') is-invalid @enderror" name="apellido" value="{{$usuario->apellido}}" required>

                        @error('apellido')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>


                    <div class="form-group">
                      <label for="calle" class="col-sm-2 control-label">Calle <font color= red><strong>*</strong></font></label>
                      <div class="col-sm-8">
                        <input id="calle" type="text" class="form-control @error('calle') is-invalid @enderror" name="calle" value="{{$usuario->calle}}" required>

                        @error('calle')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="numero" class="col-sm-2 control-label">Numero <font color= red><strong>*</strong></font></label>
                      <div class="col-sm-8">
                        <input id="numero" type="text" class="form-control  @error('numero') is-invalid @enderror" name="numero" value="{{$usuario->numero}}" required>

                        @error('numero')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="cuit_cuil" class="col-sm-2 control-label">Cuit/Cuil</label>
                      <div class="col-sm-8">
                        <input id="cuit_cuil" type="text" class="form-control @error('cuit_cuil') is-invalid @enderror" name="cuit_cuil" value="{{$usuario->cuit_cuil}}">

                        @error('cuit_cuil')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="tel_fijo" class="col-sm-2 control-label">Telefono Fijo</label>
                      <div class="col-sm-8">
                        <input id="tel_fijo" type="text" class="form-control @error('tel_fijo') is-invalid @enderror" name="tel_fijo" value="{{$usuario->tel_fijo}}">

                        @error('tel_fijo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Celular</label>
                      <div class="col-sm-8">
                        <input id="tel_celular" type="text" class="form-control @error('tel_celular') is-invalid @enderror" name="tel_celular" value="{{$usuario->tel_celular}}">

                        @if ($errors->has('tel_celular'))
                            <p>{{ $errors->first('tel_celular') }}</p>
                        @endif

                      </div>
                    </div>

                    

                
                        <div class="form-group">
                        <label class="col-lg-2 control-label">Localidad <font color= red><strong>*</strong></font></label>
                        <div class="col-sm-8">
                          <select name="id_localidad" class="form-control">
                            <option selected disabled>Seleccione una localidad</option>
                                @foreach($localidades as $localidad)
                                    <option value={{$localidad->id_localidad}} @if($localidad->id_localidad==$usuario->id_localidad) selected @endif>{{$localidad->nombre}}</option>
                                @endforeach
                          </select>
                          @if ($errors->has('id_localidad'))
                              <p>{{ $errors->first('id_localidad') }}</p>
                          @endif
  
                        </div> 
                          
                        </div>
                      

                      

                      <div class="form-group">
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2">
                            <input type="submit" class="btn btn-primary  btn-lg"  value="Guardar">
                        </div>
                     </div>

                  </form>
                </div>
              </section>
           </div>
    </div>

    
     
    
</section>
</section>


@endsection

