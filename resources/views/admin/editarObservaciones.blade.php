@extends('admin.layout')

@section('content')
 
<section id="main-content" >
    <section class="wrapper">

        <div class="row">

        <div class="col-md-12">
       

              <header class="panel-heading">
                @foreach ($mascota as $mascotaUsuario)
                <h3>Historia clinica de {{$mascotaUsuario->nombre}}</h3>
                @endforeach

              </header>

              <div class="row">
              <div class="panel body">
                <form  method="POST" action="{{url('/guardarObservaciones')}}" enctype="multipart/form-data">
                    @csrf
                   @foreach ($mascota as $mascotaUsuario)
                       <input type="hidden" name="id_mascota" id="id_mascota" value={{$mascotaUsuario->id_mascota}}>

                        <div class="form-group">
                                <div class="col-sm-8">
                                    <textarea readonly class="form-control" name="observaciones" rows="5" placeholder="Historia Clinica">{{$mascotaUsuario->observaciones}}</textarea>
                                </div>
                        </div>
                    
                   @endforeach
               </form>
            </div>
            </div>

            <div class="row">
              <div class="panel body">
                    <form  method="POST" action="{{url('/guardarObservaciones')}}" enctype="multipart/form-data">
                        @csrf
                       @foreach ($mascota as $mascotaUsuario)
                           <input type="hidden" name="id_mascota" id="id_mascota" value={{$mascotaUsuario->id_mascota}}>

                                                
                            <div class="form-group">
                                    <div class="col-sm-8">
                                        <textarea  class="form-control" type="imput" name="observaciones" rows="15" id="observaciones" value="{{$mascotaUsuario->observaciones}}" >{{$mascotaUsuario->observaciones}}</textarea>
                                    </div>
                            </div>
                    
                            
                                <label class="col-sm-8 control-label"></label>
                                    <div class="col-sm-2">
                                        <input type="submit" class="btn btn-primary  btn-lg" value="Guardar cambios"> 
                                </div>
                          
                             
                       @endforeach
                   </form>
                </div> 
            </div>         
                  
                <div class="row">         
                    <label class="col-sm-10 control-label"></label> 
                    <div class="col-sm-2">
                        @foreach ($mascota as $mascotaUsuario)
                        <a href="{{ route('getMascotasUsuario',$mascotaUsuario->id_usuario)}}" class="btn btn-primary">
                            Volver             
                        </a>
                        @endforeach
                 </div>
                  </div>
            
                </div>
            </section>

     
        </div>
        </div>
  
</section>


@endsection

