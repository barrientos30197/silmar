@extends('admin.layout')

@section('content')
 
<section id="main-content" >
    <section class="wrapper">

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                    Registrar vacuna: 
                    </header>
                    <div class="panel-body">
                     <form action="{{ route('registrarVacuna')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                       {{ csrf_field()}}
          

                       @if ($vacuna != NULL)
                        <input type="hidden" id="id_vacuna" name="id_vacuna" value={{$vacuna->id_vacuna}}>
                        @endif
                       <div class="form-group">
                        <label class="col-lg-2 control-label">Mascota</label>
                        <div class="col-sm-8">
                          @foreach ($mascota as $m)
                            {{$m->nombre}}                        
                            <input type="hidden" id="id_mascota" name="id_mascota" value={{$m->id_mascota}} required>
                          @endforeach
                        </div>
                      </div>   

                       <div class="form-group">
                         <label class="col-lg-2 control-label"> Marca</label>
                         <div class="col-sm-8">
                            @if ($vacuna != NULL) 
                              <input type="text" class="form-control" name="marca" id="marca" value={{$vacuna->marca}}>
                            @else
                              <input type="text" class="form-control" name="marca" id="marca" > 
                            @endif
                          </div>
                       </div>
   
                                     
                       <div class="form-group">
                        <label class="col-lg-2 control-label">Serie</label>
                        <div class="col-sm-8">
                        @if ($vacuna != NULL)
                          <input type="text" class="form-control" name="serie" id="serie" value={{$vacuna->serie}} >
                        @else
                        <input type="text" class="form-control" name="serie"  >
                        @endif 
                        </div> 
                      
                      </div> 

                      <div class="form-group">
                         <label class="col-lg-2 control-label"> Descripción</label>
                         <div class="col-sm-8">
                            @if ($vacuna != NULL)
                            <input type="text" class="form-control" name="descripcion" id="descripcion" value={{$vacuna->descripcion}}>
                            @else
                            <input type="text" class="form-control" name="descripcion"  >
                            @endif
                         </div>
                       </div>

                      <div class="form-group">
                        <label class="col-lg-2 control-label">Fecha <font color= red><strong>*</strong></font></label>
                        <div class="col-sm-8">
                        @if ($vacuna != NULL)
                          <input type="date" class="form-control" value={{$vacuna->fecha_vacunacion}} name="fecha_vacunacion" >
                        @else
                          <input type="date" class="form-control" name="fecha_vacunacion"  required>
                        @endif
                        </div> 
                      
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Fecha del refuerzo </label>
                        <div class="col-sm-8">
                        @if ($vacuna != NULL)
                          <input type="date" class="form-control" value={{$vacuna->fecha_revacunacion}} name="fecha_revacunacion" >
                        @else
                          <input type="date" class="form-control" name="fecha_revacunacion" >
                        @endif  
                      </div> 
                      
                      </div>
                      <div class="form-group">
                        <label class="col-sm-10 control-label"></label>
                            <div class="col-sm-2">
                           <input type="submit" class="btn btn-primary  btn-lg" value="Guardar">
                        </div>
                     </div>
                   </div>
                      
                      </form>
                      </div>
                      @if(session('vacunaCreada'))
                        <span></span>
                       <div class="alert-success form-control"> {{ session('vacunaCreada') }}
                       </div>
                      @endif
                      @if(session('vacunaNoCreada'))
                            <span></span>
                          <div class="alert-danger form-control"> {{ session('vacunaNoCreada') }}</div>
                      @endif
               
 
                </section>
               </div>

        </div>

        <div style="text-align:center">
        @foreach ($mascota as $m)                                          
          <a href="{{route('getVacunas', $m->id_mascota)}}" class="btn btn-primary btn-lg">
            Ver Vacunas de {{$m->nombre}}  
          </a>
        @endforeach
      </div>
      
    </section>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
      $('#selectUsuario').on('change', function(){
          var seleccionado=$('#selectUsuario').val();
          var urla='{{route('listaMascotas')}}';
          var selOpts = "";

          document.getElementById('id_mascota').removeAttribute("disabled");
          $.ajax({
              type: 'GET',
              url:'listaMascotasAJAX?id_usuario=' + seleccionado, //Completo la ruta, es decir, le agrego el parametro que se necesita para la busqueda
          }).done(function(data) {
              datos = data.data; // Tuve que quedarme con data.data porque paginate(20) devuelve informacion de más y si saco  ->paginate(20); tira error
              $.each(datos, function(k, v)
            {
                var id = datos[k].id_mascota;
                var val = datos[k].nombre;
                selOpts += "<option value='"+id+"'>"+val+"</option>";
            });
            $('#id_mascota').append(selOpts);
          });
      });
      $('#id_mascota').on('change', function(){
      console.log($('#id_mascota').val());
    });
  </script>
@endsection
