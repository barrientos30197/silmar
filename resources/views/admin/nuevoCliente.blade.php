@extends('admin.layout')

@section('content')
 
<section id="main-content" >
    <section class="wrapper">

        <div class="row">
            <div class="col-lg-12">
              <h3 class="page-header"><i class="fa fa-file-text-o"></i> Nuevo Cliente: </h3>
             
            </div>
         </div>

    <div class="row">
           <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                  Datos del cliente: 
                </header>
                <div class="panel-body">
                  <form action="{{route('crearUsuario')}}" method="POST" enctype="multipart/form-data"class="form-horizontal">
                    @csrf
                    <div class="form-group">
                      <label for="nombre" class="col-sm-2 control-label">Nombre <font color= red><strong>*</strong></font></label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre"> 

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="apellido" class="col-sm-2 control-label">Apellido<font color= red><strong>*</strong></font></label>
                      <div class="col-sm-8">
                        <input id="apellido" type="text" class="form-control @error('apellido') is-invalid @enderror" name="apellido" value="{{ old('apellido') }}" required autocomplete="apellido" autofocus>

                        @error('apellido')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="dni" class="col-sm-2 control-label">Dni<font color= red><strong>*</strong></font></label>
                      <div class="col-sm-8">
                        <input id="dni" type="text" class="form-control @error('dni') is-invalid @enderror" name="dni" required>

                                @error('dni')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="calle" class="col-sm-2 control-label">Calle <font color= red><strong>*</strong></font></label>
                      <div class="col-sm-8">
                        <input id="calle" type="text" class="form-control @error('calle') is-invalid @enderror" name="calle" value="{{ old('calle') }}" required autocomplete="calle" autofocus>

                        @error('calle')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="numero" class="col-sm-2 control-label">Numero <font color= red><strong>*</strong></font></label>
                      <div class="col-sm-8">
                        <input id="numero" type="text" class="form-control @error('numero') is-invalid @enderror" name="numero" value="{{ old('numero') }}" required autocomplete="numero" autofocus>

                                @error('numero')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="cuit_cuil" class="col-sm-2 control-label">Cuit/Cuil</label>
                      <div class="col-sm-8">
                        <input id="cuit_cuil" type="text" class="form-control @error('cuit_cuil') is-invalid @enderror" name="cuit_cuil" value="{{ old('cuit_cuil') }}" autocomplete="cuit_cuil" autofocus>

                                @error('cuit_cuil')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="tel_fijo" class="col-sm-2 control-label">Telefono Fijo</label>
                      <div class="col-sm-8">
                        <input id="tel_fijo" type="text" class="form-control @error('tel_fijo') is-invalid @enderror" name="tel_fijo" value="{{ old('tel_fijo') }}" autocomplete="tel_fijo" autofocus>

                        @error('tel_fijo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Celular</label>
                      <div class="col-sm-8">
                        <input id="tel_celular" type="text" class="form-control @error('tel_celular') is-invalid @enderror" name="tel_celular" value="{{ old('tel_celular') }}" autocomplete="tel_celular" autofocus>

                        @error('tel_celular')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                      </div>
                    </div>

                    

                
                        <div class="form-group">
                        <label class="col-lg-2 control-label">Localidad <font color= red><strong>*</strong></font></label>
                        <div class="col-sm-8">
                          <select name="id_localidad" class="form-control">
                            <option selected disabled>Seleccione una localidad</option>
                                @foreach($localidades as $localidad)
                                    <option value={{$localidad->id_localidad}}>{{$localidad->nombre}}</option>
                                @endforeach
                          </select>
                          @if ($errors->has('id_localidad'))
                              <p>{{ $errors->first('id_localidad') }}</p>
                          @endif
  
                        </div> 
                          
                        </div>
                      

                      <div class="form-group">
                        <label for="password" class="col-lg-2 control-label">{{ __('Password') }}</label>
                        <div class="col-sm-8">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                          
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-2 control-label">{{ __('Confirm Password') }}</label>
                        <div class="col-sm-8">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                          
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2">
                            <input type="submit" class="btn btn-primary  btn-lg"  value="Guardar">
                        </div>
                     </div>

                  </form>
                </div>
                
                  {{-- Estos 2 if son mensajes que retornamos desde el backend para mostrar al administrador mensajes correctos o con errors
                  que tenga ?? '' significa que la variable es opcional--}}
                  @if(session('usuarioCreadoCorrectamente'))
                        <span></span>
                       <div class="alert-success form-control"> {{ session('usuarioCreadoCorrectamente') }}</div>

                  @endif
                  @if(session('usuarioNoCreado'))
                  
                  <div class="form-group">
                    <span>
                  <div class=" alert-danger">{{ session('usuarioNoCreado')}}</div>

                  @endif

                  
              </section>
           </div>
          
    </div>

</section>
</section>


@endsection

