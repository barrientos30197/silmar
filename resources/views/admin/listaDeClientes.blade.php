@extends('admin.layout')

@section('content')
 
<section id="main-content" >
    <section class="wrapper">

        <div class="row">

           <div class="col-md-12">
            <div class="form-group">

            <section class="panel">
              <header class="panel-heading">
                <h3>Clientes</h3>

              </header>

              <div class="panel body">
                <table class="table">
                  <thead>
                    <tr>
                      <th style="text-align: end"><h4><strong>Buscar</strong></h4></th>

                      <th><input id="form-input" class="form-control" placeholder="Ingrese el apellido del cliente" type="text"> </th>

                      {{-- El icono de buscar lo sacaria porque al hacerlo con ajax no va a ser necesario apretarlo --}}
                      <th>
                        <a onclick="buscarUsuarioAJAX();"  class="btn btn-primary btn-sm"><i class="fa fa-search fa-2x"></i></a>
                      </th>
                    </tr>
                            
              
                  </thead>
                </table>  
              </div> 
            
              <div class="panel-body">
                <table class="table  table-striped">
                  <thead>
                    
                    <tr>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Dni</th>
                      <th>Cuit/Cuil</th>
                      <th>Telefono Fijo</th>
                      <th>Celular</th>
                      <th>Localidad</th>
                      <th>Calle</th>
                      <th>Numero</th>
                      <th>Mascotas</th>
                      <th>Editar</th>
                      <th>Eliminar</th>
                     
                      
                    </tr>
                  </thead>
                 <tbody id="tbody">
                    @foreach ($usuarios as $usuario)
                       <tr>  
                        <th>{{$usuario->nombre}} </th>
                        <th>{{$usuario->apellido}} </th>
                        <th>{{$usuario->dni}} </th>
                        <th>{{$usuario->cuit_cuil}} </th>
                        <th>{{$usuario->tel_fijo}} </th>
                        <th>{{$usuario->tel_celular}} </th>
                        <th>{{$usuario->localidad}} </th>
                        <th>{{$usuario->calle}} </th>
                        <th>{{$usuario->numero}} </th>
                        <th style="text-align: center"><a class="btn btn-primary  btn-sm" href="{{route('getMascotasUsuario',$usuario->id)}}"><i class="fas fa-paw fa-2x"></i> </a></th>
                        <th style="text-align: center"><a class="btn btn-primary  btn-sm" href="{{route('editarUsuarioView',$usuario->id)}}"><i class="far fa-edit fa-2x"></i></a></th>
                        <th style="text-align: center">
                          <a onclick="eliminarUsuario({{$usuario->id}});"  class="btn btn-default btn-sm btn-danger"><i class="far fa-trash-alt fa-2x"></i></a>
                          <form id="eliminar{{$usuario->id}}" method="POST" action="{{route('eliminarUsuario')}}" enctype="multipart/form-data">
                              @csrf
                              <input type="hidden" name="id_usuario" value={{$usuario->id}}>
                          </form><br>
                       </th>
                      </tr>
                      
                      

                    @endforeach
                 </tbody>
                </table>
            </div>

            {{-- Estos 2 if son mensajes que retornamos desde el backend para mostrar al administrador mensajes correctos o con errors
          que tenga ?? '' significa que la variable es opcional--}}
            @if($mensajeOk ?? '')
        
              {{ $mensajeOk }}
            @endif

            @if($mensajeError ?? '')
                {{ $mensajeError }}
            @endif
            
        </section>
        </div>
         </div>
         <div class="col-md-12" style="text-align:center">
          {{$usuarios->render()}}
         </div>

</section>
</section>

@endsection

@section('scripts')
  <script type="text/javascript">
    //Funcion que envia el formulario si el admin acepta la confirmacion de eliminar al usuario seleccionado
    function eliminarUsuario(id_usuario){
        if(confirm("Esta seguro de eliminar este usuario")){
            document.getElementById("eliminar"+id_usuario).submit();
        }
    }

      /*
        Esta funcion se activa al escribir en el input con id form-input y va recibiendo lo que se escribe 
        en el input y lo almacena en la variable input.
        urla es la ruta a la cual se llamara con AJAX.
        Si el texto no esta vacio, vamos guardando el texto ingresado para ir buscando y si el texto esta
        vacio llamamos a la funcion para que retorne nuevamente todos los usuarios.
        Al resultado siempre se lo reemplazamos al body de la tabla.
      */
      $('#form-input').on('keyup', function(){
          var texto="";
          var input=$('#form-input').val();
          var urla='{{route('buscarUsuarioAJAX')}}';
          
          
          if(input!=''){ //Si ingreso texto en el input
              texto=$('#form-input').val();
          }
          else if(input==''){ //Si no ingreso, vuelvo a mostrar todos
              urla='{{route('listarUsuariosAJAX')}}';
          }

          $.ajax({
              type: 'GET',
              url: urla + '/' + texto, //Completo la ruta, es decir, le agrego el parametro que se necesita para la busqueda
          }).done(function(data) {
              $( "#tbody" ).html( data ); //El resultado de la busqueda, lo muestro en el div con id listaUsuarios
          });
      });
  </script>
@endsection