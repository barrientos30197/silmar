

@extends('admin.layout')

@section('content')
 
<section id="main-content" >
    <section class="wrapper">
    <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-file-text-o"></i> Agendar Turno: </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                    Datos del turno:
                    </header>
                    <div class="panel-body">
                    <form action="{{route('agendarTurno')}}" method="POST" onsubmit="return validar()"
                            enctype="multipart/form-data" class="form-horizontal">
                       {{ csrf_field()}}
          

                       <div class="form-group">
                        <label class="col-lg-2 control-label">Cliente<font color= red><strong>*</strong></font></label></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control has-search" onkeyup="filtrarUsuario(this.value)"
                                            placeholder="Buscar usuario" title="Ingrese nombre" />
                                            
                                    <select class="form-control" name="id_usuario" id="selectUsuario" required>
                                        <option disabled selected value="">Seleccionar un usuario</option>
                                        @if (count($usuarios) == 0)
                                        <option>No se encuentran usuarios</option>
                                        @endif
                                        @foreach ($usuarios as $usuario)
                                        <option value="{{$usuario->id}}">{{$usuario->nombre}} </option>
                                        @endforeach
                                    </select>
                        </div>
                      </div>   
                     

                      <div class="form-group">
                        <label class="col-lg-2 control-label">Fecha<font color= red><strong>*</strong></font></label>
                        <div class="col-sm-8">
                          <input type="date" class="form-control"  name="fecha_turno" required>
                        </div> 
                      </div> 
                        <div class="form-group">
                          <label class="col-lg-2 control-label">Hora <font color= red><strong>*</strong></font></label>
                          <div class="col-sm-8">
                            <input type="time" class="form-control"  name="hora_turno" value="11:45:00" max="22:30:00" min="10:00:00" step="1" required>
                          </div> 
                        </div>
  
                          <div class="form-group">
                          <label class="col-lg-2 control-label">Descripción </label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control"  name="descripcion" required>
                          </div> 
                          </div>
                    
                        

                      <div class="form-group">
                        <div class="col-sm-10">
                        @if(session('turnoCreado'))
                        <span></span>
                       <div class="alert-success form-control"> {{ session('turnoCreado') }}
                       </div>
                      @endif
                      @if(session('turnoNoCreado'))
                            <span></span>
                          <div class="alert-danger form-control"> {{ session('turnoNoCreado') }}</div>
                      @endif
                        </div>
                            <div class="col-sm-2">
                           <input type="submit" class="btn btn-primary  btn-lg" value="Guardar">
                        </div>
                     </div>
                   </div>
                      
                      </form>
                      </div>
                     
               
 
                </section>
               </div>

        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
    </section>
</section>
@endsection

