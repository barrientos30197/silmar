@extends('admin.layout')

@section('content') 
  <section id="main-content" >
      <section class="wrapper">
      @if(session('vacunaEliminada'))
                    <span></span>
                    <div class="alert-success form-control" style="text-align:center; font-size:120%"> 
                        {{ session('vacunaEliminada') }}
                    </div>                  
                    <br>
                @elseif(session('vacunaNoEliminada'))
                    <span></span>
                    <div class="alert-danger form-control"> {{ session('vacunaNoEliminada') }}</div>
                    <br>
                @endif
          <div class="panel-body">
              <header class="panel-heading"  style="font-size:120%"><i class="fas fa-capsules"></i>
                Vacunas de 
                @foreach ($mascota as $m)
                    {{$m->nombre}} 
                    <?php $idMascota = $m->id_mascota ?>                           
                @endforeach
              </header>    
              <table class="table  table-striped">
                <thead>
                  <tr>
                   <th> Descripción</th>
                    <th>Marca</th>
                    <th>Serie</th>
                    <th>Fecha</th>
                    <th>Revacunacion</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                  </tr>
                </thead>
                <tbody id="tbody">
                  @foreach ($vacunas as $vacuna)
                      <tr>  
                      <td>{{$vacuna->descripcion}} </td>
                        <td>{{$vacuna->marca}} </td>
                        <td>{{$vacuna->serie}} </td>
                        <td>{{$vacuna->fecha_vacunacion}} </td>
                        <td>{{$vacuna->fecha_revacunacion}} </td>            
                        <td>                                                                                        
                            <a href="{{route('cargarVacuna',  [$vacuna->id_vacuna, $idMascota])}}" class="btn btn-primary btn-sm">
                                <i class="fas fa-pencil-alt" style="font-size:18px"></i>
                            </a>                                            
                        </td>
                        <td>
                            <a onclick="eliminarVacuna({{$vacuna->id_vacuna}})" href="#" class="btn btn-danger">
                                <i class="fas fa-trash-alt" style="font-size:18px"></i>
                            </a>  
                            <form id="eliminar{{$vacuna->id_vacuna}}" method="POST" action="{{route('eliminarVacuna')}}" enctype="multipart/form-data">
                              @csrf
                              <input type="hidden" name="id_vacuna" value={{$vacuna->id_vacuna}}>
                            </form>                                        
                        </td> 
                    </tr>  
                  @endforeach
                </tbody>
                
              </table>
              <div style="text-align:center">
                <a href="{{route('cargarVacuna', [0, $idMascota])}}" class="btn btn-primary btn-lg">
                    Agregar Nueva Vacuna                
                </a>
              </div>
              <div class="col-md-12" style="text-align:center">
                {{$vacunas->render()}}
              </div>
            </div>
      </section>
  </section>
@endsection

@section('scripts')
  <script type="text/javascript">
    function eliminarVacuna(id_vacuna){
      if(confirm("Esta seguro de eliminar esta vacuna")){
          document.getElementById("eliminar"+id_vacuna).submit();
      }
    }
  </script>
@endsection