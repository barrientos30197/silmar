@extends('admin.layout')

@section('content')
    <!--main content start-->
  
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
       
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop">  Escritorio</i></h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="{{route('home')}}">Inicio | Escritorio</a></li>
              
            </ol>
          </div>
        </div>
       
      
        <div class="row">
         
          <div class="col-lg-8 portlets">
         
            <div class="panel panel-default">
              <div class="panel-heading">
                <p>Recordatorios</p>
              </div>
              <!--ACA VENDRIA BIEN UN MARGIN EN TODOS LOS LADOS -->
              <div class="panel-body">
                <div class="form-row">
                  <div class="form-group">
                    <label for="recordatorioDiario">Recordatorio diario</label>
                    <form action={{ route('cargarRecordatorio2')}} method="post">
                      {{ csrf_field()}}
                      @if (Auth::user()->diario != null)
                        <textarea class="form-control" name="diario" style="resize: none" id="recordatorioDiario" rows="5">{{ Auth::user()->diario }}</textarea>
                      @else
                        <textarea class="form-control" name="diario" style="resize: none" id="recordatorioDiario" rows="5"></textarea>
                      @endif
                      <input type="number" name="id" value={{Auth::user()->id}} hidden>
                      <button type="submit" style="margin-top:5px;" class="btn btn-info btn-sm">Guardar</button>
                    <button type="button" style="margin-top:5px;" class="btn btn-danger btn-sm" data-toggle="modal" value={{Auth::user()->id}} id="recordatorioDiarioButton" data-target="#modalEliminar">Borrar</button>
                    </form>
                  </div>
                  <form action={{ route('cargarRecordatorio2')}} method="post">
                    {{ csrf_field()}}
                    <div class="form-group col-md-6" style="padding-left:0px !important;">
                      <label for="recordatorioSemanal">Recordatorio semanal</label>
                      @if (Auth::user()->semanal != null)
                        <textarea class="form-control" name="semanal" style="resize: none" id="recordatorioSemanal" rows="5">{{Auth::user()->semanal}}</textarea>
                      @else
                        <textarea class="form-control" name="semanal" style="resize: none" id="recordatorioSemanal" rows="5"></textarea>
                      @endif
                      <input type="number" name="id" value={{Auth::user()->id}} hidden>
                      <button type="submit" style="margin-top:5px;" class="btn btn-info btn-sm">Guardar</button>
                      <button type="button" style="margin-top:5px;" class="btn btn-danger btn-sm" data-toggle="modal" value={{Auth::user()->id}} id="recordatorioSemanalButton" data-target="#modalEliminar">Borrar</button>
                    </div>
                  </form>
                  <form action={{ route('cargarRecordatorio2')}} method="post">
                    {{ csrf_field()}}
                    <div class="form-group col-md-6" style="padding-right:0px !important;">
                      <label for="recordatorioMensual">Recordatorio mensual</label>
                      @if(Auth::user()->mensual != null)
                        <textarea class="form-control" name="mensual" style="resize: none" id="recordatorioMensual" col rows="5">{{Auth::user()->mensual}}</textarea>
                      @else
                        <textarea class="form-control" name="mensual" style="resize: none" id="recordatorioMensual" col rows="5"></textarea>
                      @endif
                      <input type="number" name="id" value={{Auth::user()->id}} hidden>
                      <button type="submit" style="margin-top:5px;" class="btn btn-info btn-sm">Guardar</button>
                      <button type="button" style="margin-top:5px;" class="btn btn-danger btn-sm" data-toggle="modal" value={{Auth::user()->id}} id="recordatorioMensualButton" data-target="#modalEliminar">Borrar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          

         <!--Project Activity start-->
         <section class="panel">
          <div class="panel-body progress-panel">
            <div class="row">
              <div class="col-lg-3 task-progress pull-left">
               <H1> <strong>VACUNAS PARA LA SEMANA: </strong> </H1> <br><br>
                   
                  <table class="table  table-striped">
                    <thead>
                      <TR>
                        <th>Fecha</th>
                        <th>Mascota</th>
                        <th>Dueño</th>
                        
                      </TR> 
                   
                    </thead>

                   <tbody id="tbody">
                     @foreach ($vacunasProgramadas as $vacunas)
                         <tr>  
                          <th>{{$vacunas->fecha_revacunacion}} </th>
                          <th>{{$vacunas->nombreMascota}}</th>
                          <th>{{$vacunas->nombre}} {{$vacunas->apellido}} </th>  
                         </tr>
                      @endforeach
                      
                   </tbody>
                  </table> 
                </div>
          
              </div>
            </div>
            </section>
      </div>
      
     
     
       
      </section>
    </section>

    <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminar" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                  <h5 class="modal-title" id="exampleModalLongTitle">Eliminar recordatorio</h5>
              </div>
              <div class="modal-body">
                  <h3 class="modal-title" >¿Desea limpiar el recordatorio?</h3><br>
                  <p  id="parrafoEliminar"> </p><br>
                  <form id="formularioModal" action="" method="get">
                      <div id="divBotonModal">
                        
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  
  <script type="text/javascript" src={{ asset('js/recordatorios.js')}}></script>
          
  
 
@endsection


