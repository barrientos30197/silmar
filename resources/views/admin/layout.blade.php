<!DOCTYPE html>
<html leng="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Veterinaria Sil Mar">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Sistema, gestion, veterinaria,sil mar, veterinarios, pequeños animales, animales, mascotas">
  <link rel="shortcut icon" href="template/img/favicon.png">
  <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

  <title>Veterinaria SIL-MAR</title>

  <!-- Bootstrap CSS -->
  <link href="{{ asset('template/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="{{ asset('template/css/bootstrap-theme.css') }}" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="{{ asset('template/css/elegant-icons-style.css') }}" rel="stylesheet" />
  <link href="{{ asset('template/css/fontawesome/css/all.css') }}" rel="stylesheet" />
  <!-- full calendar css-->
  <link href="{{ asset('template/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css') }}" rel="stylesheet" />
  <link href="{{ asset('template/assets/fullcalendar/fullcalendar/fullcalendar.css') }}" rel="stylesheet" />
  <!-- easy pie chart-->
  <link href="{{ asset('template/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css') }}" rel="stylesheet" type="text/css" media="screen" />
  <!-- owl carousel -->
  <link rel="stylesheet" href="{{ asset('template/css/owl.carousel.css') }}" type="text/css">
  <link href="{{ asset('template/css/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
  <!-- Custom styles -->
  <link rel="{{ asset('template/stylesheet" href="css/fullcalendar.css') }}">
  <link href="{{ asset('template/css/widgets.css') }}" rel="stylesheet">
  <link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('template/css/style-responsive.css') }}" rel="stylesheet" />
  <link href="{{ asset('template/css/xcharts.min.css') }}" rel=" stylesheet">
  <link href="{{ asset('template/css/jquery-ui-1.10.4.min.css') }}" rel="stylesheet">
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css'>

</head>

<body>
  <!-- container section start -->
  <section id="container" class="" >


    <header class="header dark-bg">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>

      <!--logo start-->
      <a href="{{route('home')}}" class="logo"><strong>Veterinaria<strong><span class="lite">Sil-Mar</span></strong></a>
      <!--logo end-->

    



      
      <div class="top-nav notification-row">
          <!-- user login dropdown start-->
        <ul class="nav pull-right top-menu">
          
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <span class="profile-ava">
                  <img alt="" src="{{ asset('template/img/avatar.png') }}">
              </span>
              <span class="username"><strong>Dr. {{Auth::user()->nombre}} {{Auth::user()->apellido}}</strong></span>
              <b class="caret"></b>
           </a>
          
            
            <ul class="dropdown-menu extended logout">
                <div class="log-arrow-up"></div>
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  <i class="icon_key_alt"></i> Log Out
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </header>
    <!--header end-->

    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="{{ (request()->is('home')) ? 'active' : '' }}">
            <a class="" href="{{route('home')}}">
              <i class="icon_house_alt"></i>
              <span>Inicio | Escritorio</span>
            </a>
          </li>
          <li class="{{ (request()->is('listaDeClientes')) ? 'active' : '' }}">
            <a href="{{route('listaDeClientes')}}" class="">
              <i class="icon_search"></i>
              <span>Mis clientes</span>  
            </a>
            
          </li>
          <li class="{{ (request()->is('crearUsuario')) ? 'active' : '' }}">
            <a href="{{route('crearUsuario_view')}}" class="">
              <i class="icon_document_alt"></i>
              <span>Nuevo cliente</span>
            </a>
          </li>
          <li class="{{ (request()->is('nuevaMascota')) ? 'active' : '' }}">
            <a href="{{route('cargarMascota')}}" class="">
              <i class="fas fa-paw"></i>
              <span>Nueva mascota</span>
            </a>
          </li>
          <li class="{{ (request()->is('getTurnosPorDia')) ? 'active' : '' }}">
            <a href="{{route('getTurnosPorDia')}}" class="">
              <i class="fa fa-clock"></i>
              <span>Turnos</span>
            </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

    <div class="content">            
        @yield('content')
    </div>
  </section>


  <!-- javascripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>  
  <script src="{{ asset('template/js/jquery.js') }}"></script>
  <script src="{{ asset('template/js/jquery-ui-1.10.4.min.') }}"></script>
  <script src="{{ asset('template/js/jquery-1.8.3.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('template/js/jquery-ui-1.9.2.custom.min.js') }}"></script>
  <!-- bootstrap -->
  <script src="{{ asset('template/js/bootstrap.min.js') }}"></script>
  <!-- nice scroll -->
  <script src="{{ asset('template/js/jquery.scrollTo.min.js') }}"></script>
  <script src="{{ asset('template/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="{{ asset('template/assets/jquery-knob/js/jquery.knob.js') }}"></script>
  <script src="{{ asset('template/js/jquery.sparkline.js') }}" type="text/javascript"></script>
  <script src="{{ asset('template/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}"></script>
  <script src="{{ asset('template/js/owl.carousel.js') }}"></script>
  <!-- jQuery full calendar -->
  <script src="{{ asset('template/js/fullcalendar.min.js') }}"></script>
    <!-- Full Google Calendar - Calendar -->
    <script src="{{ asset('template/assets/fullcalendar/fullcalendar/fullcalendar.js') }}"></script>
    <!--script for this page only-->
    <script src="{{ asset('template/js/calendar-custom.js') }}"></script>
    <script src="{{ asset('template/js/jquery.rateit.min.js') }}"></script>
    <!-- custom select -->
    <script src="{{ asset('template/js/jquery.customSelect.min.js') }}"></script>
    <script src="{{ asset('template/assets/chart-master/Chart.js') }}"></script>

    <!--custome script for all page-->
    <script src="{{ asset('template/js/scripts.js') }}"></script>
    <!-- custom script for this page-->
    <script src="{{ asset('template/js/sparkline-chart.js') }}"></script>
    <script src="{{ asset('template/js/easy-pie-chart.js') }}"></script>
    <script src="{{ asset('template/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('template/js/xcharts.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.autosize.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.placeholder.min.js') }}"></script>
    <script src="{{ asset('template/js/gdp-data.js') }}"></script>
    <script src="{{ asset('template/js/morris.min.js') }}"></script>
    <script src="{{ asset('template/js/sparklines.js') }}"></script>
    <script src="{{ asset('template/js/charts.js') }}"></script>
    <script src="{{ asset('template/js/jquery.slimscroll.min.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
    <script>
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });
    </script>
    @yield('scripts')

</body>

</html>
