@if(count($usuarios)>0)
    @foreach ($usuarios as $usuario)
        <tr>  
            <th>{{$usuario->nombre}} </th>
            <th>{{$usuario->apellido}} </th>
            <th>{{$usuario->dni}} </th>
            <th>{{$usuario->cuit_cuil}} </th>
            <th>{{$usuario->tel_fijo}} </th>
            <th>{{$usuario->tel_celular}} </th>
            <th>{{$usuario->localidad}} </th>
            <th>{{$usuario->calle}} </th>
            <th>{{$usuario->numero}} </th>

            <th><a class="btn btn-info btn-sm" href="{{route('getMascotasUsuario',$usuario->id)}}"><i class="fas fa-paw fa-2x"></i></a></th>
            <th style="text-align: center"><a class="btn btn-primary  btn-sm" href="{{route('editarUsuarioView',$usuario->id)}}"><i class="fas fa-pencil-alt fa-2x"></i></a></th>
            <th style="text-align: center">
            <a onclick="eliminarUsuario({{$usuario->id}});"  class="btn btn-default btn-sm"><i class="fas fa-trash-alt fa-2x"></i></a>
            <form id="eliminar{{$usuario->id}}" method="POST" action="{{route('eliminarUsuario')}}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id_usuario" value={{$usuario->id}}>
            </form><br>
            </th>
        </tr>    
    @endforeach
@else
    <tr><th colspan="11">No se encontraron usuarios con ese apellido</th></tr>
@endif
