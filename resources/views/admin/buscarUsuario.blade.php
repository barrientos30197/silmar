@extends('admin.layout')

@section('content')
 {{--ACA HAY QUE PONER LA RUTA DESDE EL LAYOUT--}}
<section id="main-content" >
    <section class="wrapper">

        <div class="row"> <div class="col-md-12">
            <section class="panel">
              <header class="panel-heading">
                <h3>Clientes</h3>

              </header>
              <div class="panel-body">
                <table class="table  table-striped">
                  <thead>
                    <tr>
                      <th >Nombre</th>
                      <th>Apellido</th>
                      <th>Dni</th>
                      <th>Cuit</th>
                      <th>Telefono Fijo</th>
                      <th>Celular</th>

                      {{--FALTA LA TABLA LOCALIDAD <th>Localidad</th>--}}

                      
                      <th>Calle</th>
                      <th>Numero</th>
                      <th>Editar</th>
                      <th>Eliminar</th>
                      
                    </tr>
                  </thead>
                 <tbody>
                  <div id="listaUsuarios">
                      
                       @if(count($usuarios)>0)
                    @foreach ($usuarios as $usuario)
                       <tr>  
                        <th>{{$usuario->nombre}} </th>
                        <th>{{$usuario->apellido}} </th>
                        <th>{{$usuario->dni}} </th>
                        <th>{{$usuario->cuit_cuil}} </th>
                        <th>{{$usuario->tel_fijo}} </th>
                        <th>{{$usuario->tel_celular}} </th>
                       {{--}} <th>{{$usuario->localidad}} </th>--}}
                        <th>{{$usuario->calle}} </th>
                        <th>{{$usuario->numero}} </th>
                         
                        <th style=" text-align: center"><a class="btn btn-primary  btn-sm" href="{{route('editarUsuarioView',$usuario->id)}}"><i class="fa fa-pencil fa-2x"></i></a></th>
                       <th style=" text-align: center"> <a onclick="eliminarUsuario({{$usuario->id}});" class="btn btn-default btn-sm"><i class="fa fa-trash-o fa-2x"></i></a>
                        <form id="eliminar{{$usuario->id}}" method="POST" action="{{route('eliminarUsuario')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_usuario" value={{$usuario->id}}>
                        </form><br>
                        @endforeach
                        @else
                        No se encontraron usuarios con ese DNI
                    @endif

                       </th>
                      </tr>    

         </div>
    </section>
</section>

@endsection
