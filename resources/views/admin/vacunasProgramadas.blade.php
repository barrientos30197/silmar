@extends('admin.layout')

@section('content')
    <section id="main-content" >
        <section class="wrapper">

            <div class="row">
                @foreach($vacunasProgramadas as $vacuna)
                    {{--Estan todos los datos de las vacunas, solo muestro la fecha para ver si anda todo bien--}}
                    {{$vacuna->fecha_vacunacion}}
                @endforeach
            </div>
        </section>
    </section>
@endsection