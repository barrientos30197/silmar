<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
 
  <link rel="shortcut icon" href="template/img/favicon.png">

  <title>Login | Sil Mar</title>

  <!-- Bootstrap CSS -->
  <link href="{{ asset('template/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="{{ asset('template/css/bootstrap-theme.css' ) }}"  rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="{{ asset('template/css/elegant-icons-style.css' ) }}" rel="stylesheet" />
  <link href="{{ asset('template/css/font-awesome.css' ) }}" rel="stylesheet" />
  <!-- Custom styles -->
  <link href="{{ asset('template/css/style.css' ) }}"  rel="stylesheet">
  <link href="{{ asset('template/css/style-responsive.css' )}}" rel="stylesheet"/>

 

 
</head>

<body class="login-img3-body">

  <div class="container">

    <form class="login-form" action="{{ route('login') }}" method="POST">
      @csrf
      <div class="login-wrap">
        <p class="login-img"><i class="icon_lock_alt"></i></p>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_profile"></i></span>
         
            <input id="dni" type="dni" class="form-control @error('dni') is-invalid @enderror"  placeholder="Usuario" autofocus   name="dni" value="{{ old('dni') }}" required autocomplete="dni" autofocus>

            @error('dni')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
      
          
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_key_alt"></i></span>
          
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Contraseña" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
        </div>
        <label class="checkbox">
                <input type="checkbox" value="remember-me"> Recordarme?
                <span class="pull-right"> <a href="#"> Recordar la contraseña</a></span>
            </label>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Ingresar</button>
            </div>
    </form>
    
  </div>


</body>

</html>
