@extends('usuario.layout')

@section('content')
  <!--main content start-->  

  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><i class="icon_heart_alt"></i> Mis Mascotas</h3>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="{{route('home')}}">Inicio</a></li>        
        <li><i class="icon_heart_alt"></i>Mis Mascotas</li>  
        @foreach ($mascota as $a)      
          <li><i class="fa fa-paw"></i>{{ $a->nombre }}</li> 
        @endforeach
      </ol>
    </div>
  </div> 


  <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 col-sm-6">
            <div class="panel">
                <header class="panel-heading">Datos</header>
                <div class="panel-body">
                    <div class="col-lg-6">
                        <!-- Muestro los datos de la mascota -->
                        @foreach ($mascota as $a) 
                          <div class="row"><label><b>Nombre: </b> {{ $a->nombre }} </label></div>
                          <div class="row"><label><b>Especie: </b> {{ $a->especie }} </label></div>
                          <div class="row"><label><b>Raza: </b> {{ $a->raza }} </label></div>
                          <div class="row"><label><b>Sexo: </b> 
                          @if ( $a->sexo == 1)
                            Macho  </label></div>
                          @else
                            Hembra  </label></div>
                          @endif
                          <div class="row"><label><b>Color: </b> {{ $a->color }}  </label></div>
                          <div class="row"><label><b>Peso: </b> {{ $a->peso }} kg</label></div>
                          <div class="row"><label><b>Historia clinica: </b> "{{ $a->observaciones}}"</label></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3"></div>
    </div>

      <div class="row">        
        <div class="col-lg-6 col-sm-6">
            <div class="panel">
                <header class="panel-heading">Vacunas</header>
                <div class="panel-body">                    
                  <table class="table">
                    <thead>
                      <tr>                        
                        <td>Marca </td>
                        <td>Serie </td>
                        <td>Fecha vacunación </td>
                        <td>Fecha Refuerzo</td>                                                  
                      </tr>
                    </thead>
                    <tbody id="tbody">
                      @foreach ($vacunas as $vacuna) 
                        <tr>
                          <th>{{ $vacuna->marca }}</th>
                          <th>{{ $vacuna->serie }}</th>
                          <th>{{ $vacuna->fecha_vacunacion }}</th>
                          <th>{{ $vacuna->fecha_revacunacion }}</th>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>                    
                </div>
            </div>
        </div>        

      <div class="row">        
        <div class="col-lg-6 col-sm-6">
            <div class="panel">
                <header class="panel-heading">Antiparasitarios</header>
                <div class="panel-body">                    
                  <table class="table">
                    <thead>
                      <tr>                        
                        <td>Marca </td>
                        <td>Dosis </td>
                        <td>Fecha Antiparasitario </td>
                        <td>Fecha Renovación</td>                                                  
                      </tr>
                    </thead>
                    <tbody id="tbody">
                      @foreach ($antiparasitarios as $antiparasitario) 
                        <tr>
                          <th>{{ $antiparasitario->marca }}</th>
                          <th>{{ $antiparasitario->dosis }}</th>
                          <th>{{ $antiparasitario->fecha_desparasitacion }}</th>
                          <th>{{ $antiparasitario->fecha_renovacion }}</th>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>                    
                </div>
            </div>
        </div>  
        

@endsection
