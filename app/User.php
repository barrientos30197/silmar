<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    public $timestamps = false;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'apellido', 'dni', 'calle', 'numero', 'cuit_cuil', 'tel_fijo', 'tel_celular', 'id_localidad', 'diario', 'semanal', 'mensual', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];

    public function routeNotificationForWhatsApp(){
        //if($this->tel_celular){
        //    return "+5492954615046";
        //}
        return "+5492302209165";
    }

    public function routeNotificationForNexmo($notification)
    {
        return $this->tel_celular;
    }
}
