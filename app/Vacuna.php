<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacuna extends Model
{
    public $timestamps = false;
    //

    /* Tuve que agregar para que tome la tabla Desparasitaciones y la primary key
    ya que no me las reconocía */
    
    public $table = "vacunas";
    protected $primaryKey = 'id_vacuna';
}
