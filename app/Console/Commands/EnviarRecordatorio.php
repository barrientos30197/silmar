<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;
use App\Vacuna;
use App\User;
use DateTime;

use Illuminate\Support\Facades\Auth;
use App\Notifications\Recordatorio;
use App\Notifications\RecordatorioSMS;
use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\Notifiable;

class EnviarRecordatorio extends Command
{
    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enviar:recordatorios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar un recordatorio por WhatsApp del cliente cuando la fecha de vacunacion se aproxime';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $vacunaciones = Vacuna::all();
        foreach ($vacunaciones as $vacunacion){
            $fecha_actual = date("Y-m-d");

            $diferencia = $this->calcularDiferenciaDeDias($vacunacion->fecha_vacunacion, $fecha_actual);

            //Si faltan 5 dias para la vacunacion, le envio un mensaje
            if($diferencia == 5){
                $this->enviarWhatsapp($vacunacion);
            }
        }
    }
    
    public function enviarWhatsapp($vacunacion){
        $idMascota=$vacunacion->id_mascota;
        $idUsuario=DB::table('mascotas')->select('id_usuario')->where('id_mascota','=',$idMascota)->first()->id_usuario;
        $celular=DB::table('users')->select('tel_celular')->where('id','=',$idUsuario)->first()->tel_celular;
        $celular=(int)$celular;
        if($celular != 0){
            $usuario = DB::table('users')->where('id',$idUsuario)->first();
            $user = new User((array)$usuario);
            $user->notify(new RecordatorioSMS());
        }
 
        //Comando para probar todo: php artisan enviar:recordatorios
    }

    /* 
        Esta funcion verifica si hay que hacer los calculos para ver la diferencia de dias 
        entre la fecha de vacunacion y la actual.
    */
    private function calcularDiferenciaDeDias($fecha_vac,$fecha_actual){
        $fecha_vac_aux = new DateTime($fecha_vac);
        $fecha_actual_aux = new DateTime($fecha_actual);

        //Si todavia falta para le fecha de vacunacion, entonces calculo la diferencia de dias.
        if($fecha_vac_aux > $fecha_actual_aux){
            $dif = $fecha_actual_aux->diff($fecha_vac_aux); //El orden del calculo no importa, siempre devuelve la misma diferencia.
            return $dif->days;
        }
        else{
            return -1;
        }
    }
}
