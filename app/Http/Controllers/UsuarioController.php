<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;


class UsuarioController extends Controller
{
    /*
        Esta funcion se encarga de enviar la vista al navegador.
    */
    public function crearUsuario_view(){
        $localidades = DB::table('localidades')->get();
        $pelajes = DB::table('pelajes')->get();
        $tallas = DB::table('tallas')->get();
        return view('admin.nuevoCliente',compact('localidades','tallas','pelajes'));
    }

    /*
        Esta funcion se encarga de recibir todos los datos necesarios para cargar un nuevo usuario.
    */
    public function crearUsuario(Request $request){
        
        //Llamamos a la funcion para validar los datos ingresados
        $this->validarDatos($request);
        try{
            $usuario=new User;

            //Llamo a la funcion privada para guardar el usuario, y le paso el usuario y los datos
            $this->guardarUsuario($usuario,$request);

            //Retornamos a la vista anterior con un mensaje que indica que el usuario fue creado correctamente
            return back()->with("usuarioCreadoCorrectamente","Usuario creado correctamente.");  
        }
        catch(Exception $e){
            //Retornamos a la vista anterior con un mensaje que indica que el usuario no fue creado correctamente
            return back()->with("usuarioNoCreado","El usuario no se creo correctamente.");  
        }      
    }

    /*
        Esta funcion se encarga de enviar la vista de todos los usuarios.
    */
    public function listaDeClientes(){
        try{
            //Llamo a la funcion privada para obtener los usuarios con su nombre de localidad
            $usuarios = $this->usuariosConSuLocalidad();

            return view('admin.listaDeClientes',compact('usuarios'));
        }
        catch(Exception $e){
            return back()->with("errorCargarUsuarios","Se produjo un error al consultar todos los  usuarios");
        }
    }

    /*
        Esta funcion busca en la tabla usuarios a los que tengan el dni igual al que ingreso
        o que empiece con el dni ingresado y lo retorna a la vista donde se muestran los
        resultados de AJAX
    */
    public function buscarUsuarioAJAX($apellido){        
        $usuarios = DB::table('users')
            ->select('users.id','users.nombre','users.apellido','users.dni','users.calle','users.numero','users.cuit_cuil','users.tel_fijo','users.tel_celular', 'localidades.nombre as localidad')
            ->where('apellido', 'LIKE',$apellido.'%')
            ->join('localidades', 'users.id_localidad', '=', 'localidades.id_localidad')
            ->orderBy('id')
            ->get();

        return view('admin.buscarUsuarioAJAX',compact('usuarios'));
    }

    /*
        Esta funcion busca todos los usuarios, es como la de listarUsuarios pero devuelve
        todo en la vista donde se muestran los resultados de AJAX
    */
    public function listarUsuariosAJAX(){
        //Llamo a la funcion privada para obtener los usuarios con su nombre de localidad
        $usuarios = $this->usuariosConSuLocalidad();
        return view('admin.buscarUsuarioAJAX',compact('usuarios'));
    }
    
    /*
        Esta funcion recibe el id del usuario para eliminarlo.
    */
    public function eliminarUsuario(Request $request){
        try{
            $usuario= User::find($request->id_usuario);
            $usuario->delete();

            //Llamo a la funcion privada para obtener los usuarios con su nombre de localidad
            $usuarios = $this->usuariosConSuLocalidad();

            return view('admin.listaDeClientes')->with(["mensajeOk" => "Usuario eliminado correctamente", "usuarios" => $usuarios]);
        }
        catch(Exception $e){
    
            return view('admin.listaDeClientes')->with(["mensajeError" => "Hubo un error al eliminar el usuario", "usuarios" => DB::table('users')->get()]);
        }
    }

    /*
        Esta funcion se encarga de direccionar la vista, en donde se modificaran los campos del usuario que se haya seleccionado,al navegador.
    */  
    public function editarUsuarioView($id_usuario){
        try{
            $usuario = User::find($id_usuario);
            $localidades = DB::table('localidades')->get();
            return view('admin.editarUsuario',compact('usuario','localidades'));/* tambien tendria que mandarle las localidades*/ 
        }
        catch(Exception $e){
            return back()->with("errorAlModificar","Ocurrio un error al buscar el usuario que deseaba editar");
        }
    }

    /*
        Esta funcion se encarga de recibir los datos necesarios para realizar la actualizacion del usuario.
    */
    public function editarUsuario(Request $request){
        try{
            $usuario = User::find($request->id_usuario);

            //Llamamos a la funcion para validar los datos ingresados
            $this->validarDatos($request);

            //Llamo a la funcion privada para guardar el usuario, y le paso el usuario y los datos
            $this->guardarUsuario($usuario,$request);

            //Llamo a la funcion privada para obtener los usuarios con su nombre de localidad
            $usuarios = $this->usuariosConSuLocalidad();

            return view('admin.listaDeClientes')->with(["mensajeOk" => "Usuario editado correctamente", "usuarios" => $usuarios]);
        }
        catch(Exception $e){
            return back()->with("mensajeError","Hubo un error al editar el usuario");
        }
    }

    /* 
        Lo que hace esta funcion es recibir al usuario creado o encontrado para editar y los datos que se deben almacenar.
        Es una funcion privada hecha para no tener que poner el mismo codigo para crear un usuario y para editarlo.
    */
    private function guardarUsuario($usuario,$request){
            $usuario->nombre=$request->nombre;
            $usuario->apellido=$request->apellido;
            /*Capas haya que cambiar este if pero lo que hace es ver si la contraseña esta vacia*/
            if(!($usuario->password!="")){
                $usuario->password=Hash::make($request->password);
            }   
            $usuario->dni=$request->dni;
            $usuario->calle=$request->calle;
            $usuario->numero=$request->numero;
            $usuario->cuit_cuil=$request->cuit_cuil;
            $usuario->tel_fijo=$request->tel_fijo;
            $usuario->tel_celular=$request->tel_celular;
            $usuario->id_localidad=$request->id_localidad;
            
            $usuario->save(); //Con esto, guardamos al usuario, no importa si es nuevo o si lo estamos actualizando
    }


/*  esta funcion busca  un usuario y sus mascotas a partir de un id */
/* y  los manda a la vista "infoClienteView" */
    public function getUsuario($idUsuario){
        $usuario =  DB::table('users') //join table users and table user_details base from matched id;
        ->select('users.id','users.nombre','users.apellido','users.dni','users.calle','users.numero','users.cuit_cuil','users.tel_fijo','users.tel_celular', 'localidades.nombre as localidad')
        ->join('localidades', 'users.id_localidad', '=', 'localidades.id_localidad')
        ->where("users.id",$idUsuario)
        ->get(); 
        $mascotasUsuario =DB::table('mascotas')->where([['id_usuario', $idUsuario]])->get(); 
        $turnos = DB::table('turnos')->where([['id_usuario', $idUsuario]])->get(); 
        return view('usuario.home',compact('usuario','mascotasUsuario', 'turnos'));                             
    }

    /*
        Esta funcion recibe los datos ingresados para crear el nuevo usuario, o editarlo y los valida.
    */
    private function validarDatos($request){
        $request->validate(
            [
                'nombre' => ['required', 'string','max:45'],
                'apellido' => ['required', 'string','max:45'],
                'password' => ['required', 'string','min:8'],
                'dni' => ['required', 'string'],
                'calle' => ['required', 'string'],
                'numero' => ['required', 'string'],
                'cuit_cuil' => ['nullable', 'string', 'min:9','max:11'],
                'tel_fijo' => ['nullable', 'string','max:10'],
                'tel_celular' => ['nullable', 'string','max:12'],
                'id_localidad' => ['required']                  
            ],
            [
                'nombre' => 'El nombre es requerido y puede contener hasta 45 letras y/o numeros.',
                'apellido' => 'El apellido es requerido y puede contener hasta 45 letras y/o numeros.',
                'password' => 'La contraseña es requerida y debe tener al menos 8 caracteres.',
                'dni' => 'El numero de dni es requerido.',
                'calle' => 'La calle es requerida y puede contener hasta 45 letras y/o numeros.',
                'numero' => 'El numero es requerido y puede contener hasta 10 numeros.',
                'cuit_cuil' => 'El cuit/cuil debe tener entre 9 y 11 caracteres.',
                'tel_fijo' => 'El numero de telefono fijo puede tener hasta 10 digitos.',
                'tel_celular' => 'El numero de telefono celular puede tener hasta 15 digitos.',
                'id_localidad' => 'La localidad es requerida.'
            ]
        );
    }

    /* 
        Esta funcion retorna a todos los usuarios con su respectiva localidad (el nombre) 
    */
    public function usuariosConSuLocalidad(){                
        $usuarios = DB::table('users')
            ->select('users.id','users.nombre','users.apellido','users.dni','users.calle','users.numero','users.cuit_cuil','users.tel_fijo','users.tel_celular', 'localidades.nombre as localidad')
            ->join('localidades', 'users.id_localidad', '=', 'localidades.id_localidad')
            ->where([['users.administrador', false]])
            ->orderBy('id')
            ->paginate(15);
        return $usuarios;                             
    }


///dado el id de un usuario, devuele todas las mascotas que este tenga
public function getMascotasUsuario($id_usuario)
{
    $mascotas = DB::table('mascotas')->where([['id_usuario', $id_usuario]])->paginate(15); 
    $usuario = User::find($id_usuario)->nombre;  
    return view('admin.mascotasUsuario', compact('mascotas','usuario'));
}

}

