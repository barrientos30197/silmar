<?php

namespace App\Http\Controllers;

use App\Turno;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class TurnosController extends Controller
{

    public function agendarTurnoView(){
        $usuarios = DB::table('users')->get();
        return view('admin.nuevoTurno', compact('usuarios'));
    }

    /* Carga o edita un turno */
    public function agendarTurno(Request $request){
        try{      
            if ($request->id_turno!=NULL)
                $turno= Turno::find($request->id_turno);      
            else
                $turno=new Turno;            
            $turno->id_usuario = $request->id_usuario;  
            $turno->fecha_turno = $request->fecha_turno;
            $turno->hora_turno = $request->hora_turno;
            $turno->descripcion = $request->descripcion;
            $turno->save(); 

            return back()->with("turnoCreado","¡Turno agendado!");  
        }
        catch(Exception $e){           
            return back()->with("turnoNoCreado","Error al agendar el turno, inténtelo de nuevo.");  
        }      
    }

    /*  Retorna todos los turnos 
    */
    public function getTurnos(){
        $turnos =  DB::table('turnos')->get();                 
        return $turnos;
        return view('admin.turnos',compact('turnos'));
    }

    /*  Retorna un turno en dado su id. */
    public function getTurno($id_turno){
        $turno = Turno::find($id_turno);        
        return $turno;
        return view('admin.turno',compact('turno'));
    }

    /*  Cancelar turno */
    public function eliminarTurno(Request $request){
        try{
            $turno= Turno::find($request->id_turno);
            $turno->delete();
            return response()->json(['success'=>'true' , 'mensaje'=>'Se ha eliminado correctamente el turno.']);
        }
        catch(Exception $e){
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al eliminar el turno.']);
        }      
    }

    public function getTurnosPorDia(){
        return view("admin.verTurnos");
    }

    public function getTurnosPorDiaAJAX($fecha){
        try{
            $turnos = Turno::where("fecha_turno",$fecha)->orderBy('hora_turno','asc')->get();
            foreach ($turnos as $turno){
                $turno->cliente = User::find($turno->id_usuario)->nombre;
            }
            return response()->json(['success'=>'true' , 'turnos' => $turnos]);
        }
        catch(Exception $e){
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al obtener los turnos.']);
        }
    }
}

