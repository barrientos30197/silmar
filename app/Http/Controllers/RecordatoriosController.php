<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RecordatoriosController extends Controller
{

    /*  Esta función serviría para el front como está ahora, con un solo formulario y 3 botones que hacen submit a la misma función. 
        Carga los recordatorios que sean distintos de NULL, ya que sino sobreescribe con NULL lo que ya está cargado.*/
    public function cargarRecordatorio2(Request $request){
        try{      
            if ($request->diario != NULL){            
                $usuario = User::find($request->id); //Podrías pasarle el id del Auth por un input de tipo hidden             
                $usuario->diario = $request->diario;        
                $usuario->save(); 
            }
            if ($request->semanal != NULL){   
                $usuario = User::find($request->id);
                $usuario->semanal = $request->semanal;
                $usuario->save(); 
            }
            if ($request->mensual != NULL){   
                $usuario = User::find($request->id);
                $usuario->mensual = $request->mensual;                          
                $usuario->save(); 
            }

            return back()->with("recordatorioCreado","Recordatorio creado correctamente.");  
        }
        catch(Exception $e){           
            return back()->with("recordatorioNoCreado","No se pudo almacenar el recordatorio correctamente.");  
        }      
    }

    /*  Elimina el recordatorio según el tipo (diario/semanal/mensual). */
    public function eliminarRecordatorio($id_usuario, $tipo){
        try{
            $usuario = User::find($id_usuario);            
            $usuario->$tipo = NULL;                   
            $usuario->save(); 
            return back()->with('recordatorioEliminado', 'Los datos de la antiparasitación se eliminaron correctamente');
         }catch (Exception $e) {
             return back()->with('recordatorioNoEliminado', 'Error al eliminar los datos la antiparasitación');
         }        
    }

}




