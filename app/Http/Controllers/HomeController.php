<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if (Auth::user()->administrador == 0) {
            return redirect()->route('getUsuario', [Auth::user()->id]);
        } else {
            //echo get_class($request->user());
            $vacunasController = new VacunasController();
            $vacunasProgramadas= $vacunasController->getVacunasProgramadas();
            return view('admin.index', compact('vacunasProgramadas'));
        }
    }
}
