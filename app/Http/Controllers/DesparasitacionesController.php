<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Desparasitacion;
use Illuminate\Support\Facades\DB;


class DesparasitacionesController extends Controller
{

    /**se usa esto para cargar las desparasitaciones 
    *hasta que se cree la vista  que muestre  las mascotas que tiene un  usuario */
    public function cargarDesparasitacion($id_desparasitacion=0, $id_mascota){
        
        $mascota =DB::table('mascotas')->select('nombre', 'id_mascota') 
        ->where('id_mascota',$id_mascota)
        ->get(); 
        
        if($id_desparasitacion!=0)
            $desparasitacion = Desparasitacion::find($id_desparasitacion);            
        else
            $desparasitacion =NULL;        

        return view('admin.desparasitaciones',compact('mascota','desparasitacion')); 
    }



/* se encargar de recibir datos de una desparasitacion, ya sea una nueva o los datos de una ya existente para poder modificarla*/ 
    public function nuevaDesparasitacion(Request $request){
        try{
            if($request->id_desparasitacion!="")
            {
                $desparasitacion = Desparasitacion::find($request->id_desparasitacion);
            }
            else{
                $desparasitacion=new Desparasitacion;
            }

            $this->setDesparasitacion($request,$desparasitacion);            
            return back()->with("desparasitacionCreada","Datos de la antiparasitación almacenados correctamente.");  
        }
        catch(Exception $e){           
            return back()->with("desparasitacionNocreada","No se pudo almacenar los datos de la antiparasitación correctamente.");  
        }      
    }


    
    /** carga los datos de una desparasitacion realizada a una mascota en la BD */ 
    private function setDesparasitacion($request, $desparasitacion){             
         $desparasitacion->marca = $request->marca;
         $desparasitacion->dosis = $request->dosis;
         $desparasitacion->id_mascota = $request->id_mascota;
         $desparasitacion->fecha_desparasitacion = $request->fecha_desparasitacion;
         $desparasitacion->fecha_renovacion = $request->fecha_renovacion;/* creo que se renuevan cada cierta cantidad de meses */                 
         $desparasitacion->save();           
    }


    /** retorna  todos las desparasitaciones de una mascota a la vista 'desparasitaciones'*/
    public function getDesparasitaciones($idMascota){
        $desparasitaciones =  DB::table('desparasitaciones') 
        ->select('id_desparasitacion','marca','dosis','fecha_desparasitacion','fecha_renovacion')
        ->where("id_mascota",$idMascota)
        ->paginate(15); 

        /* esto es para que se muestre el nombre de la mascota en la tabla de sus desprasitaciones */
        $mascota=DB::table('mascotas') 
         ->select('nombre', 'id_mascota')
         ->where("id_mascota",$idMascota)
         ->get(); 
 
        return view('admin.verDesparasitaciones',compact('desparasitaciones', 'mascota'));/*cambia la vista de retorno cuando se agregue la vista de las amscotas del usuario*/
    }


    public function nuevaDesparasitacionView($idMascota){
        return view('admin.verDesparasitaciones',compact('desparasitaciones', 'mascota'));/*cambia la vista de retorno cuando se agregue la vista de las amscotas del usuario*/
    }



    /* se elimina una desparasitacion */
    public function eliminarDesparasitacion($id_desparasitacion){
        try{
            $desparasitacion= Desparasitacion::find($id_desparasitacion);
            $desparasitacion->delete();
            return back()->with('desparasitacionEliminada', 'Los datos de la antiparasitación se eliminaron correctamente');
         }catch (Exception $e) {
             return back()->with('desparasitacionNoEliminada', 'Error al eliminar los datos la antiparasitación');
         }        
    }


}




