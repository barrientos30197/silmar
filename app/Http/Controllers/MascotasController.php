<?php

namespace App\Http\Controllers;

use App\Mascota;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class mascotasController extends Controller
{
     /*
        Esta funcion se encarga de devolver los pelajes y sus medidas.
    */
    function getPelajes()
    {
        $pelajes = DB::table('pelajes')->select('id_pelaje', 'medida')->get();
        return $pelajes;
    }

    /*
        Esta funcion envia a la vista de cargar mascota.
    */
    function cargarMascota(Request $request)
    {
        //tengo que tomar todos las opciones de pelaje, sexo, raza, etc.
        $pelajes = self::getPelajes();
        $tallas = self::tallas();
        $usuarios = DB::table('users')
            ->select('users.dni','users.nombre','users.apellido')            
            ->where([['users.administrador', false]])
            ->orderBy('users.apellido')
            ->get();

        return view('admin.nuevaMascota', compact('pelajes', 'tallas', 'usuarios'));
    }
    /*
        Esta funcion se encarga de devolver las mascotas de un usuario dado.
    */
    /*public function listaMascotas(Request $request)
    {
        if (Auth::user()) {
            $id_usuario = $request->id_usuario;
             $usuarios= DB::table('users')->get();
             $mascotas = [];
             if($id_usuario){
            $mascotas = DB::table('mascotas')
                ->orderBy('nombre', 'asc')
                ->where([['id_usuario', $id_usuario]])
                ->paginate(20);
                }
         return view('listarMascotas',compact('mascotas','usuarios')); // Los usuarios los devuelvo para el Select

        } else {
            return redirect('/');
        }
    }*/

    public function listaMascotas($id_usuario)
    {
        $mascotas = [];
        if ($id_usuario) {
            $mascotas = DB::table('mascotas')
                ->orderBy('nombre', 'asc')
                ->where([['id_usuario', $id_usuario]])
                ->paginate(20);
        }
        return $mascotas;
    }
    
    /*
        Esta funcion se encarga de devolver las mascotas de un usuario dado a llamada AJAX.
    */
    public function listaMascotasAJAX(Request $request)
    {
        if (Auth::user()) {
            $id_usuario = $request->id_usuario;
            $mascotas = [];
            if ($id_usuario) {
                $mascotas = DB::table('mascotas')
                    ->orderBy('nombre', 'asc')
                    ->where([['id_usuario', $id_usuario]])
                    ->paginate(20);
            }
            return $mascotas;
        } else {
            return redirect('/');
        }
    }
    /*
        Esta funcion devuelve las tallas registradas en el sistema.
    */
    public function tallas()
    {
        $tallas = DB::table('tallas')
            ->select('id_talla', 'medida')
            ->get();

        return $tallas;
    }
    /*
        Esta funcion se encarga de agregar una nueva mascota en el sistema.
    */
    function nuevaMascota(Request $request)
    {
        $datos = request()->except('_token'); //TOMO TODOS LOS DATOS MENOS EL TOKEN QUE NO INTERESA
        try {
            //tengo que buscar el id del usuario que tenga ese documento
            $idUsuario = DB::table('users')->select('id')->where('dni', '=', $datos['dniCliente'])->first();
            $datos['id_usuario'] = $idUsuario->id;
            $datos['peso'] = number_format($datos['peso'], 2);
            //TENGO QUE ELIMINAR EL DNI DEL CLIENTE
            unset($datos['dniCliente']);
            //return $datos;
            Mascota::insert($datos);
            return back()->with('mensaje_ok', 'Mascota cargada correctamente');
        } catch (\Throwable $th) {
            return back()->with('mensaje_error', 'Error al cargar la mascota');
        }
    }

    /*
    Esta funcion devuelve la mascota que corresponde a un id de mascota dado.
    */
    //public function getMascota(Request $request){
    //    if (Auth::user()) {
    //        $id_mascota = $request->id_mascota;
    //        $mascota = DB::table('mascotas')
    //            ->where([['id_mascota', $id_mascota]]);
    //        return $mascota;
    //    }
    //}

    public function getMascota($id_mascota)
    {
        $mascota = DB::table('mascotas')
            ->where([['id_mascota', $id_mascota]])->get();     
        $vacunas = DB::table('vacunas')
            ->where([['id_mascota', $id_mascota]])->get();
        $antiparasitarios = DB::table('desparasitaciones')
            ->where([['id_mascota', $id_mascota]])->get();
        return view('usuario.mascota', compact('mascota', 'vacunas', 'antiparasitarios'));
    }

    /*
    Esta funcion modifica las mascotas, cambia el nombre, el sexo y/o el dueño porque son 
    los atributos que pueden llegar a cambiar dentro del dominio.
    */
    public function modificarMascota(Request $request)
    {
        echo $request->nombre;
        if (Auth::user()) {
            try {
                $id = $request->id;
                if ($request->nombre != null || $request->nombre != "") {
                    DB::table('mascotas')
                        ->where([['id_mascota', $id]])
                        ->update(
                            ['nombre' => $request->nombre]
                        );
                }
                if ($request->sexo != null || $request->sexo != "") {
                    DB::table('mascotas')
                        ->where([['id_mascota', $id]])
                        ->update(
                            ['sexo' => $request->sexo]
                        );
                }
                if ($request->id_usuario != null || $request->id_usuario != "") {
                    DB::table('mascotas')
                        ->where([['id_mascota', $id]])
                        ->update(
                            ['id_usuario' => $request->id_usuario]
                        );
                }
                if ($request->peso != null || $request->peso != 0) {
                    DB::table('mascotas')
                        ->where([['id_mascota', $id]])
                        ->update(
                            ['peso' => $request->peso]
                        );
                }
                return back()->with('mensaje_ok', 'Mascota modificada correctamente');
            } catch (\Throwable $th) {
                return back()->with('mensaje_error', 'Error al modificar mascota');
            }
        }
    }

    /*
        Esta funcion se encarga de eliminar una mascota dado un id.
    */
    function eliminarMascota(Request $request)
    {
        try {
            $id = $request['id'];
            DB::table('mascotas')->where('id_mascota', '=', $id)->delete();
            return back()->with('mensaje_ok', 'Mascota eliminada correctamente');
        } catch (\Throwable $th) {
            return back()->with('mensaje_error', 'Error al eliminar mascota');
        }
    }


    
/** redirecciona al usuario a la vista de edicion de observaciones */
    public function editarObservaciones($id_mascota)
    {
    
         $mascota= DB::table('mascotas')->where([['id_mascota', $id_mascota]])->get();
        
            return view('admin.editarObservaciones',compact('mascota'));
        
       
    }

/* guarda las observaciones  */
public function guardarObservaciones(Request $request)
{
     
    try {
         
        
        DB::table('mascotas')->where([['id_mascota',$request->id_mascota]])->update( ['observaciones' => $request->observaciones]);

        $mascota= DB::table('mascotas')->where([['id_mascota', $request->id_mascota]]);
        $mascotas = DB::table('mascotas')->where([['id_usuario', $mascota->id_usuario]]); 
        $usuario = DB::table('user')->where([['id_usuario', $mascota->id_usuario]])->nombre;  
        
        return view('admin.mascotasUsuario', compact('mascotas','usuario'));

    } catch (\Throwable $th) {
        return back()->with('mensaje_error', 'Error al guardar los cambios');
    }
   
}

}
