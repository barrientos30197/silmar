<?php

namespace App\Http\Controllers;

use App\Vacuna;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\MascotasController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VacunasController extends Controller{
    public function cargarVacuna($id_vacuna=0, $id_mascota){
        $mascota =DB::table('mascotas')->select('nombre', 'id_mascota') 
        ->where('id_mascota',$id_mascota)
        ->get(); 
        if($id_vacuna!=0)
            $vacuna = Vacuna::find($id_vacuna);            
        else
            $vacuna=NULL;

        return view('admin.nuevaVacuna',compact('mascota','vacuna')); 
    }

    public function registrarVacuna(Request $request){ // $datos debe ser marca, serie, id_mascota, fecha_vacunacion
        try{
            if($request->id_vacuna!="")
            {
                $vacuna = Vacuna::find($request->id_vacuna);
            }
            else{
                $vacuna=new Vacuna;
            }

            $this->setVacuna($request,$vacuna);            
            return back()->with("vacunaCreada","Datos de la vacuna almacenados correctamente.");  
        }
        catch(Exception $e){           
            return back()->with("vacunaNoCreada","No se pudo almacenar los datos de la vacuna correctamente.");  
        }   
    }

    private function setVacuna( $request, $vacuna){
        $vacuna->id_vacuna = $request->id_vacuna;
        $vacuna->marca = $request->marca;
        $vacuna->descripcion = $request->descripcion;
        $vacuna->serie = $request->serie;
        $vacuna->fecha_revacunacion = $request->fecha_revacunacion;
        $vacuna->id_mascota = $request->id_mascota;
        $vacuna->fecha_vacunacion = $request->fecha_vacunacion;
        $vacuna->save();
    }

    public function getVacunas($id_mascota){
        $vacunas =  DB::table('vacunas') //join table users and table user_details base from matched id;
            ->select('id_vacuna', 'marca', 'serie' ,'descripcion','fecha_vacunacion', 'fecha_revacunacion')
            ->where("id_mascota", $id_mascota)
            ->paginate(15); 
        
        $mascota=DB::table('mascotas') 
            ->select('nombre', 'id_mascota')
            ->where("id_mascota",$id_mascota)
            ->get(); 

        return view('admin.getVacunas', compact('vacunas','mascota'));
    }

    public function nuevaVacunaView($idMascota){
        return view('admin.getVacunas',compact('vacunas', 'mascota'));/*cambia la vista de retorno cuando se agregue la vista de las amscotas del usuario*/
    }

    function eliminarVacuna(Request $request){
        try {
            $id = $request['id_vacuna'];
            DB::table('vacunas')->where('id_vacuna', '=', $id)->delete();
            return back()->with('vacunaEliminada', 'Vacuna eliminada correctamente');
        } catch (\Throwable $th) {
            return back()->with('vacunaNoEliminada', 'Error al eliminar vacuna');
        }
    }

    function getVacunasProgramadas(){
        $vacunasProgramadas = DB::select("select users.nombre, users.apellido, users.tel_celular, mascotas.nombre AS nombreMascota,vacunas.fecha_revacunacion from vacunas, users, mascotas where users.id=mascotas.id_usuario and mascotas.id_mascota=vacunas.id_mascota and fecha_revacunacion <= DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) and fecha_revacunacion >= CURRENT_DATE");

        return $vacunasProgramadas;
    }
}
