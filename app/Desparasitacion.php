<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desparasitacion extends Model
{
    public $timestamps = false;
    //

    /* Tuve que agregar para que tome la tabla Desparasitaciones y la primary key
    ya que no me las reconocía */
    
    public $table = "desparasitaciones";
    protected $primaryKey = 'id_desparasitacion';
}
