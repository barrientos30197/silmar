<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    public $timestamps = false;
    protected $primaryKey = "id_turno";
}
