<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nombre' => 'Martin',
            'apellido' => 'Barrientos',
            'password' => bcrypt('martin1234'),
            'administrador' => 1,
            'id_localidad' => 1,
            'calle' => '307',
            'numero' => '49',
            'dni' => '39933049'
        ]);
        DB::table('mascotas')->insert([
            'nombre' => 'chicho',
            'especie' => 'canino',
            'raza' => 'calle cruza cordon',
            'id_pelaje' => 1,
            'id_talla' => 1,
            'sexo' => 1,
            'peso' => 23.51,
            'color' => 'marroncito',
            'id_usuario' => 1
        ]);
    }
}
