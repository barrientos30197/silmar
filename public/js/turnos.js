$(document).ready(function () {
    $("#datepicker").datepicker({ 
          autoclose: true, 
          todayHighlight: true
    }).datepicker('update', new Date());
  });


function verificaAfiliado() {
    $('#cuilAfiliado').on('blur', function () {
        var cuil = $('#cuilAfiliado').val();
        $.ajax({
            url: '/checkAfiliado?cuil=' + cuil,
            success: function (data) {
                $('#cuilHelp').text(data);
            }
        });
    });
}

function internadoCampo() {
    $('#internacion').on('change', function () {
        toggleStatus();
    });
}

function toggleStatus() {
    if ($('#internacion').is(':checked')) {
        $('#datosInternacion :input').removeAttr('disabled');


    } else {
        $('#datosInternacion :input').attr('disabled', true);
    }
}
function filtrarUsuario(keyword) {
    var select = document.getElementById("selectUsuario");
    var encuentra = false;
    for (var i = 0; i < select.length; i++) {
        var txt = select.options[i].text;
        var include = txt.toLowerCase().startsWith(keyword.toLowerCase());
        select.options[i].style.display = include ? 'list-item' : 'none';
        if (include)
            encuentra = true;
    }
    if (!encuentra)
        select.options[0].text = "No se encuentran usuarios con ese filtro de búsqueda";
}



