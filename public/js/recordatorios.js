$(document).ready(function () {
    $('#modalEliminar').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        idUsuario=$(button).val();
        tipo=""
        
        if(button.attr("id") == 'recordatorioDiarioButton'){
            console.log('DIARIO')
            tipo="diario";
        }else{
            if(button.attr("id") == 'recordatorioSemanalButton'){
                console.log('SEMANAL')
                tipo="semanal";
            }else{
                if(button.attr("id") == 'recordatorioMensualButton'){
                    console.log('MENSUAL')
                    tipo="mensual";
                }
            }
        }
        //ACA TIENE QUE IR UN BOTON DE ACEPTAR O CANCELAR
        formAction=$('#formularioModal').attr('action','/eliminarRecordatorio/'+idUsuario+'/'+tipo)
        $('#divBotonModal').html(
            '<button class="btn btn-danger btn-sm"type="submit" id="modalButton">Aceptar</button>'
        );
    });
});

function limpiar(idTextArea){
    $('#'+idTextArea).val('');
}